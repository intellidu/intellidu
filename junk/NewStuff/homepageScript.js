//Global Variables
var moveCarouselNext = 5000;			//milliseconds - for automated carousel	


$(document).ready(function() {
	$('.signUp').click(function() {
		$('.signUp-menu').toggle();
		document.getElementById("signUpEmail").focus();
	});
	$('.logIn').click(function() {
		$('.logIn-menu').toggle();
		document.getElementById("logInUsername").focus();
	});
	
	
	
	
	
	
	//Carousel
	//Automate Carousel
	//Auto move the carousel every 5 seconds
	var t = setInterval(function(){
		moveCarouselRight();
	},moveCarouselNext);
	
	function moveCarouselRight() {
		var currentSlide = $(".active-slide");
		var nextSlide = currentSlide.next();
		var currentDot = $(".active-dot");
		var nextDot = currentDot.next();
		
		if (nextSlide.length == 0) {
			nextSlide = $(".slide").first();
			nextDot = $(".dot").first();
		}
		
		currentSlide.fadeOut(600).removeClass("active-slide");
		nextSlide.fadeIn(600).addClass("active-slide");
		
		currentDot.removeClass("active-dot");
		nextDot.addClass("active-dot");
	}
	
	function moveCarouselLeft() {
		var currentSlide = $(".active-slide");
		var prevSlide = currentSlide.prev();
		var currentDot = $(".active-dot");
		var prevDot = currentDot.prev();
		
		if (prevSlide.length == 0) {
			prevSlide = $(".slide").last();
			prevDot = $(".dot").last();
		}
		
		currentSlide.fadeOut(600).removeClass("active-slide");
		prevSlide.fadeIn(600).addClass("active-slide");
		
		currentDot.fadeOut(600).removeClass("active-dot");
		prevDot.fadeIn(600).addClass("active-dot");
	}
	
    
    $('.arrow-next').click(function() {
        moveCarouselRight();
		
		/*
		var currentSlide = $('.active-slide');
        var nextSlide = currentSlide.next();
        var currentDot = $('.active-dot');
        var nextDot = currentDot.next();
        
        if (nextSlide.length == 0) {
            nextSlide = $('.slide').first();
            nextDot = $('.dot').first();
        }
        
        currentSlide.fadeOut(600).removeClass('active-slide');
        nextSlide.fadeIn(600).addClass('active-slide');
        
        currentDot.removeClass('active-dot');
        nextDot.addClass('active-dot');
		*/
    });
    
    $('.arrow-prev').click(function() {
        moveCarouselLeft();
		
		/*
		var currentSlide = $('.active-slide');
        var prevSlide = currentSlide.prev();
        var currentDot = $('.active-dot');
        var prevDot = currentDot.prev();
        
        if (prevSlide.length == 0) {
            prevSlide = $('.slide').last();
            prevDot = $('.dot').last();
        }
        
        currentSlide.fadeOut(600).removeClass('active-slide');
        prevSlide.fadeIn(600).addClass('active-slide');
        
        currentDot.removeClass('active-dot');
        prevDot.addClass('active-dot');
		*/
    });
});



document.onclick = removeDropdown;


//Remove dropdown menues when they're not in focus
function removeDropdown() {
	//Sign Up Dropdown
	if (!(document.getElementById("signUpEmail") === document.activeElement || document.getElementById("signUpUsername") === document.activeElement || document.getElementById("signUpPassword") === document.activeElement || document.getElementById("signUpSubmit") === document.activeElement)) {
		$('.signUp-menu').hide();
	}
	
	//Log In Dropdown
	if (!(document.getElementById("logInUsername") === document.activeElement || document.getElementById("logInPassword") === document.activeElement || document.getElementById("logInSubmit") === document.activeElement)) {
		$('.logIn-menu').hide();
	}
}