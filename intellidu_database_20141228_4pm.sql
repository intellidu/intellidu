-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 28, 2014 at 10:02 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `intellidu_data`
--
CREATE DATABASE IF NOT EXISTS `intellidu_data` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `intellidu_data`;

-- --------------------------------------------------------

--
-- Table structure for table `card_enrollments_editors`
--
-- Creation: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `card_enrollments_editors` (
`id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `editor_id` int(11) NOT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card_enrollments_editors`
--

INSERT INTO `card_enrollments_editors` (`id`, `card_id`, `editor_id`, `enrolled`) VALUES
(1, 1, 3, '2014-11-06 18:14:09'),
(2, 2, 3, '2014-11-06 18:14:14'),
(3, 3, 3, '2014-11-06 18:14:19'),
(4, 4, 3, '2014-11-06 18:14:23'),
(5, 6, 3, '2014-11-22 09:55:53'),
(6, 7, 3, '2014-11-22 09:56:07'),
(7, 9, 3, '2014-11-22 10:31:58'),
(8, 10, 3, '2014-11-22 10:34:01'),
(9, 11, 3, '2014-11-22 10:34:11'),
(10, 12, 3, '2014-11-22 10:34:19'),
(11, 13, 3, '2014-11-22 10:34:27'),
(12, 15, 3, '2014-11-30 13:10:23'),
(13, 16, 3, '2014-11-30 13:10:33'),
(14, 17, 3, '2014-11-30 13:10:38'),
(15, 18, 3, '2014-11-30 13:10:42'),
(16, 20, 3, '2014-12-07 14:15:43'),
(17, 21, 3, '2014-12-19 14:03:01'),
(18, 22, 3, '2014-12-19 14:03:07'),
(19, 23, 3, '2014-12-19 14:03:19');

-- --------------------------------------------------------

--
-- Table structure for table `card_enrollments_users`
--
-- Creation: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `card_enrollments_users` (
`id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `progress` int(11) DEFAULT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `confidence` int(11) DEFAULT NULL,
  `next_review_time` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

CREATE TABLE quizzes
(
id int NOT NULL AUTO_INCREMENT,
description varchar(255),
category int(11) NOT NULL,
img_link varchar(255),
created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (ID)
)

CREATE TABLE questions
(
id int NOT NULL AUTO_INCREMENT,
quiz_id int NOT NULL,
title varchar(255) NOT NULL,
type varchar(255) NOT NULL,
num_choices int(11) NOT NULL,
created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (ID)
)

CREATE TABLE choices
(
id int NOT NULL AUTO_INCREMENT,
question_id int NOT NULL,
title varchar(255) NOT NULL,
img_link varchar(255),
created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (ID)
)







--
-- Dumping data for table `card_enrollments_users`
--

INSERT INTO `card_enrollments_users` (`id`, `card_id`, `user_id`, `progress`, `enrolled`, `updated`, `confidence`, `next_review_time`) VALUES
(1, 1, 3, NULL, '2014-11-06 18:14:09', NULL, NULL, NULL),
(2, 2, 3, NULL, '2014-11-06 18:14:14', NULL, NULL, NULL),
(3, 3, 3, NULL, '2014-11-06 18:14:18', NULL, NULL, NULL),
(4, 4, 3, NULL, '2014-11-06 18:14:23', NULL, NULL, NULL),
(5, 6, 3, NULL, '2014-11-22 09:55:53', NULL, NULL, NULL),
(6, 7, 3, NULL, '2014-11-22 09:56:07', '2014-12-19 14:04:12', 5, '2014-12-23 03:06:12'),
(7, 9, 3, NULL, '2014-11-22 10:31:58', '2014-12-19 14:29:00', 5, '2014-12-23 03:30:56'),
(8, 10, 3, NULL, '2014-11-22 10:34:01', '2014-12-19 14:04:02', 5, '2014-12-23 03:06:02'),
(9, 11, 3, NULL, '2014-11-22 10:34:11', '2014-12-19 14:04:04', 5, '2014-12-23 03:06:04'),
(10, 12, 3, NULL, '2014-11-22 10:34:19', '2014-12-19 14:04:06', 5, '2014-12-23 03:06:06'),
(11, 13, 3, NULL, '2014-11-22 10:34:27', '2014-12-19 14:04:08', 5, '2014-12-23 03:06:08'),
(12, 15, 3, NULL, '2014-11-30 13:10:23', '2014-12-19 14:04:15', 5, '2014-12-26 11:08:15'),
(13, 16, 3, NULL, '2014-11-30 13:10:33', '2014-12-19 14:04:17', 5, '2014-12-26 11:08:17'),
(14, 17, 3, NULL, '2014-11-30 13:10:38', '2014-12-19 14:04:19', 5, '2014-12-26 11:08:18'),
(15, 18, 3, NULL, '2014-11-30 13:10:42', '2014-12-19 14:04:13', 5, '2014-12-23 03:06:13'),
(16, 20, 3, NULL, '2014-12-07 14:15:43', NULL, NULL, NULL),
(17, 21, 3, NULL, '2014-12-19 14:03:00', '2014-12-19 14:29:18', 5, '2014-12-26 11:33:17'),
(18, 22, 3, NULL, '2014-12-19 14:03:06', '2014-12-19 14:29:20', 5, '2014-12-26 11:33:19'),
(19, 23, 3, NULL, '2014-12-19 14:03:18', '2014-12-19 14:29:22', 5, '2014-12-26 11:33:22');

-- --------------------------------------------------------

--
-- Table structure for table `card_interactions`
--
-- Creation: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `card_interactions` (
`id` int(11) NOT NULL,
  `card_enrollment_id` int(11) DEFAULT NULL,
  `interaction_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `confidence` int(11) DEFAULT NULL,
  `time_front` int(11) DEFAULT NULL,
  `time_back` int(11) DEFAULT NULL,
  `number_of_flips` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card_interactions`
--

INSERT INTO `card_interactions` (`id`, `card_enrollment_id`, `interaction_time`, `confidence`, `time_front`, `time_back`, `number_of_flips`) VALUES
(1, 6, '2014-11-28 22:59:40', 1, 6269, 1599, 1),
(2, 6, '2014-11-28 23:02:39', 2, 81746, 3307, 1),
(3, 7, '2014-11-28 23:03:07', 5, 22948, 3252, 1),
(4, 8, '2014-11-28 23:03:14', 5, 5136, 1209, 1),
(5, 9, '2014-11-28 23:03:20', 5, 2648, 1401, 1),
(6, 10, '2014-11-28 23:03:28', 5, 6177, 1291, 1),
(7, 11, '2014-11-28 23:03:33', 5, 2620, 992, 1),
(8, 6, '2014-11-29 21:25:58', 1, 2437, 1328, 1),
(9, 6, '2014-11-29 21:26:41', 1, 3956, 1235, 1),
(10, 6, '2014-11-30 13:15:15', 1, 117717, 2816, 3),
(11, 12, '2014-11-30 13:15:17', 1, 1340, 800, 1),
(12, 13, '2014-11-30 13:15:19', 1, 531, 969, 1),
(13, 14, '2014-11-30 13:15:21', 1, 707, 1228, 1),
(14, 15, '2014-11-30 13:18:55', 1, 211782, 2072, 3),
(15, 6, '2014-11-30 13:19:48', 5, 12156, 1045, 1),
(16, 12, '2014-11-30 13:19:50', 5, 585, 876, 1),
(17, 13, '2014-11-30 13:19:51', 5, 529, 928, 1),
(18, 14, '2014-11-30 13:19:53', 5, 546, 617, 1),
(19, 15, '2014-11-30 13:19:54', 5, 544, 878, 1),
(20, 12, '2014-11-30 13:20:01', 5, 2752, 1076, 1),
(21, 13, '2014-11-30 13:20:03', 5, 582, 643, 1),
(22, 14, '2014-11-30 13:20:04', 5, 532, 669, 1),
(23, 7, '2014-12-19 14:04:01', 2, 8107, 7703, 2),
(24, 8, '2014-12-19 14:04:03', 5, 3361, 2235, 1),
(25, 9, '2014-12-19 14:04:05', 5, 1144, 757, 1),
(26, 10, '2014-12-19 14:04:07', 5, 1056, 1237, 1),
(27, 11, '2014-12-19 14:04:09', 5, 790, 881, 1),
(28, 6, '2014-12-19 14:04:13', 5, 2865, 1010, 1),
(29, 15, '2014-12-19 14:04:14', 5, 713, 762, 1),
(30, 12, '2014-12-19 14:04:16', 5, 577, 1390, 1),
(31, 13, '2014-12-19 14:04:20', 5, 786, 604, 1),
(32, 14, '2014-12-19 14:04:20', 5, 595, 1262, 1),
(33, 17, '2014-12-19 14:04:23', 5, 1058, 2430, 1),
(34, 18, '2014-12-19 14:04:26', 5, 932, 1294, 1),
(35, 19, '2014-12-19 14:04:27', 5, 1011, 973, 1),
(36, 7, '2014-12-19 14:28:59', 5, 4776, 1764, 1),
(37, 17, '2014-12-19 14:29:07', 5, 4058, 4639, 1),
(38, 18, '2014-12-19 14:29:08', 5, 993, 795, 1),
(39, 19, '2014-12-19 14:29:09', 5, 913, 801, 1),
(40, 17, '2014-12-19 14:29:18', 5, 3977, 1332, 1),
(41, 18, '2014-12-19 14:29:20', 5, 1027, 864, 1),
(42, 19, '2014-12-19 14:29:22', 5, 1012, 1154, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--
-- Creation: Dec 24, 2014 at 07:17 PM
-- Last update: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `cards` (
`id` int(11) NOT NULL,
  `front` varchar(1000) COLLATE latin1_general_ci DEFAULT NULL,
  `back` varchar(1000) COLLATE latin1_general_ci DEFAULT NULL,
  `skill` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `skill_group` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `subskill` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `front`, `back`, `skill`, `skill_group`, `subskill`, `created`, `updated`) VALUES
(1, 'amigo', 'friend ', '', NULL, NULL, '2014-11-06 18:14:09', '2014-11-06 18:14:26'),
(2, 'dorito', 'dorito ', '', NULL, NULL, '2014-11-06 18:14:14', '2014-11-06 18:14:26'),
(3, 'taco', 'taco ', '', NULL, NULL, '2014-11-06 18:14:18', '2014-11-06 18:14:26'),
(4, '', '', '', NULL, NULL, '2014-11-06 18:14:23', '0000-00-00 00:00:00'),
(5, 'hola', 'hello ', '', NULL, NULL, '2014-11-06 18:14:26', '0000-00-00 00:00:00'),
(6, 'Card 2 Front', 'Card 2 back ', '', NULL, NULL, '2014-11-22 09:55:53', '2014-11-22 09:56:52'),
(7, 'Why does meth explode?', 'methane ', '', NULL, NULL, '2014-11-22 09:56:06', '2014-11-22 10:36:08'),
(8, 'Card 1 front', 'card 1 back  ', '', NULL, NULL, '2014-11-22 09:56:52', '0000-00-00 00:00:00'),
(9, 'How can I make meth', 'watch breaking bad ', '', NULL, NULL, '2014-11-22 10:31:57', '2014-11-22 10:36:08'),
(10, 'What color is meth', 'white ', '', NULL, NULL, '2014-11-22 10:34:01', '2014-11-22 10:36:08'),
(11, 'what color is cool meth', 'blue ', '', NULL, NULL, '2014-11-22 10:34:11', '2014-11-22 10:36:08'),
(12, '', '', '', NULL, NULL, '2014-11-22 10:34:19', '0000-00-00 00:00:00'),
(13, '', '', '', NULL, NULL, '2014-11-22 10:34:27', '0000-00-00 00:00:00'),
(14, 'Card 1 front', 'card 1 back   ', '', NULL, NULL, '2014-11-22 10:36:08', '0000-00-00 00:00:00'),
(15, 'card 2', 'card 2 back ', '', NULL, NULL, '2014-11-30 13:10:23', '2014-11-30 13:10:49'),
(16, 'card 3', 'card 3 back ', '', NULL, NULL, '2014-11-30 13:10:33', '2014-11-30 13:10:49'),
(17, 'card 4 ', 'card 4 back ', '', NULL, NULL, '2014-11-30 13:10:38', '2014-11-30 13:10:49'),
(18, '', '', '', NULL, NULL, '2014-11-30 13:10:42', '0000-00-00 00:00:00'),
(19, 'Card 1', 'card 1 back ', '', NULL, NULL, '2014-11-30 13:10:49', '0000-00-00 00:00:00'),
(20, '', '', '', NULL, NULL, '2014-12-07 14:15:43', '0000-00-00 00:00:00'),
(21, 'Step 2', 'duggie ', '', NULL, NULL, '2014-12-19 14:02:57', '2014-12-19 14:03:23'),
(22, 'step 3', 'Be amazed. ', '', NULL, NULL, '2014-12-19 14:03:06', '2014-12-19 14:03:23'),
(23, '', '', '', NULL, NULL, '2014-12-19 14:03:16', '0000-00-00 00:00:00'),
(24, 'Step 1', 'Get your dugg on ', '', NULL, NULL, '2014-12-19 14:03:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cards_in_decks`
--
-- Creation: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `cards_in_decks` (
`id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `order_in_deck` int(11) NOT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cards_in_decks`
--

INSERT INTO `cards_in_decks` (`id`, `card_id`, `deck_id`, `order_in_deck`, `enrolled`) VALUES
(1, 1, 1, 0, '2014-11-06 18:14:09'),
(2, 2, 1, 0, '2014-11-06 18:14:14'),
(3, 3, 1, 0, '2014-11-06 18:14:18'),
(4, 4, 1, 0, '2014-11-06 18:14:23'),
(6, 7, 2, 0, '2014-11-22 09:56:06'),
(7, 9, 2, 0, '2014-11-22 10:31:57'),
(8, 10, 2, 0, '2014-11-22 10:34:01'),
(9, 11, 2, 0, '2014-11-22 10:34:11'),
(10, 12, 2, 0, '2014-11-22 10:34:19'),
(11, 13, 2, 0, '2014-11-22 10:34:27'),
(12, 15, 7, 0, '2014-11-30 13:10:23'),
(13, 16, 7, 0, '2014-11-30 13:10:33'),
(14, 17, 7, 0, '2014-11-30 13:10:38'),
(15, 18, 7, 0, '2014-11-30 13:10:42'),
(16, 20, 8, 0, '2014-12-07 14:15:43'),
(17, 21, 14, 0, '2014-12-19 14:02:58'),
(18, 22, 14, 0, '2014-12-19 14:03:06'),
(19, 23, 14, 0, '2014-12-19 14:03:16');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--
-- Creation: Dec 24, 2014 at 07:17 PM
-- Last update: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `comments` (
`id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `deck_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `subject_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deck_enrollments_editors`
--
-- Creation: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `deck_enrollments_editors` (
`id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `editor_id` int(11) NOT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deck_enrollments_editors`
--

INSERT INTO `deck_enrollments_editors` (`id`, `deck_id`, `editor_id`, `enrolled`) VALUES
(1, 1, 3, '2014-11-06 18:13:50'),
(2, 2, 3, '2014-11-22 09:20:12'),
(3, 3, 3, '2014-11-22 09:20:15'),
(4, 4, 3, '2014-11-22 09:25:47'),
(5, 5, 3, '2014-11-22 09:26:15'),
(6, 6, 3, '2014-11-22 09:54:36'),
(7, 7, 3, '2014-11-30 13:08:25'),
(8, 8, 3, '2014-12-07 14:15:18'),
(9, 9, 3, '2014-12-07 14:16:01'),
(10, 10, 3, '2014-12-07 14:16:11'),
(11, 11, 3, '2014-12-07 14:16:18'),
(12, 12, 3, '2014-12-07 14:20:37'),
(13, 13, 3, '2014-12-07 14:21:22'),
(14, 14, 3, '2014-12-19 14:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `deck_enrollments_users`
--
-- Creation: Dec 24, 2014 at 07:17 PM
-- Last update: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `deck_enrollments_users` (
`id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `progress` int(11) DEFAULT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `deck_enrollments_users`
--

INSERT INTO `deck_enrollments_users` (`id`, `deck_id`, `user_id`, `progress`, `enrolled`, `updated`) VALUES
(1, 1, 3, 0, '2014-11-06 18:13:50', NULL),
(2, 2, 3, 0, '2014-11-22 09:20:12', NULL),
(3, 3, 3, 0, '2014-11-22 09:20:15', NULL),
(4, 4, 3, 0, '2014-11-22 09:25:47', NULL),
(5, 5, 3, 0, '2014-11-22 09:26:15', NULL),
(6, 6, 3, 0, '2014-11-22 09:54:36', NULL),
(7, 7, 3, 0, '2014-11-30 13:08:25', NULL),
(8, 8, 3, 0, '2014-12-07 14:15:18', NULL),
(9, 9, 3, 0, '2014-12-07 14:16:01', NULL),
(10, 10, 3, 0, '2014-12-07 14:16:11', NULL),
(11, 11, 3, 0, '2014-12-07 14:16:18', NULL),
(12, 12, 3, 0, '2014-12-07 14:20:37', NULL),
(13, 13, 3, 0, '2014-12-07 14:21:22', NULL),
(14, 14, 3, 0, '2014-12-19 14:02:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `decks`
--
-- Creation: Dec 24, 2014 at 07:17 PM
-- Last update: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `decks` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `creator_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `decks`
--

INSERT INTO `decks` (`id`, `name`, `description`, `creator_id`, `created`, `updated`) VALUES
(1, 'Pronunciation', 'Learn the pronunciation of the basic Spanish words.', '3', '2014-11-06 18:13:50', '0000-00-00 00:00:00'),
(2, 'Introduction to Meth Making', 'Learn the basic of making meth', '3', '2014-11-22 09:20:11', '0000-00-00 00:00:00'),
(3, 'Introduction to Meth Making', 'Learn the basic of making meth', '3', '2014-11-22 09:20:15', '0000-00-00 00:00:00'),
(4, 'Sample Deck', 'sample description', '3', '2014-11-22 09:25:47', '0000-00-00 00:00:00'),
(5, 'sample deck  2', 'sample description 2', '3', '2014-11-22 09:26:14', '0000-00-00 00:00:00'),
(6, 'Meth Waars', 'Learn about the wars surrounding meth', '3', '2014-11-22 09:54:36', '0000-00-00 00:00:00'),
(7, 'This deck has cards', 'This deck has cards', '3', '2014-11-30 13:08:25', '0000-00-00 00:00:00'),
(8, 'X+5 = 10', 'First deck in algebra1', '3', '2014-12-07 14:15:18', '0000-00-00 00:00:00'),
(9, 'Deck2', 'this is the second deck in algebra  1', '3', '2014-12-07 14:16:01', '0000-00-00 00:00:00'),
(10, 'Deck3', 'this is the third deck in algebra  1', '3', '2014-12-07 14:16:11', '0000-00-00 00:00:00'),
(11, 'Deck4', 'this is the fourth deck in algebra  1', '3', '2014-12-07 14:16:18', '0000-00-00 00:00:00'),
(12, 'Algebra 1 first deck', 'first deck', '3', '2014-12-07 14:20:37', '0000-00-00 00:00:00'),
(13, 'Algebra 2 first deck', 'second deck', '3', '2014-12-07 14:21:22', '0000-00-00 00:00:00'),
(14, 'How to duggie', 'This deck will teach you how to duggie - flashcard style', '3', '2014-12-19 14:02:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `decks_in_subjects`
--
-- Creation: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `decks_in_subjects` (
`id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `decks_in_subjects`
--

INSERT INTO `decks_in_subjects` (`id`, `subject_id`, `deck_id`, `enrolled`) VALUES
(1, 11, 1, '2014-11-06 18:13:50'),
(2, 8, 2, '2014-11-22 09:20:12'),
(3, 8, 3, '2014-11-22 09:20:15'),
(4, 8, 4, '2014-11-22 09:25:47'),
(5, 8, 5, '2014-11-22 09:26:14'),
(6, 8, 6, '2014-11-22 09:54:36'),
(7, 8, 7, '2014-11-30 13:08:25'),
(8, 6, 8, '2014-12-07 14:15:18'),
(9, 6, 9, '2014-12-07 14:16:01'),
(10, 6, 10, '2014-12-07 14:16:11'),
(11, 6, 11, '2014-12-07 14:16:18'),
(12, 7, 12, '2014-12-07 14:20:37'),
(13, 7, 13, '2014-12-07 14:21:22'),
(14, 12, 14, '2014-12-19 14:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--
-- Creation: Dec 24, 2014 at 07:17 PM
-- Last update: Dec 24, 2014 at 07:17 PM
-- Last check: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `friends` (
  `user` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `friend` varchar(16) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subject_enrollments_editors`
--
-- Creation: Dec 24, 2014 at 07:17 PM
-- Last update: Dec 28, 2014 at 02:42 AM
--

CREATE TABLE IF NOT EXISTS `subject_enrollments_editors` (
`id` int(11) NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `subject_enrollments_editors`
--

INSERT INTO `subject_enrollments_editors` (`id`, `subject_id`, `editor_id`, `enrolled`) VALUES
(1, NULL, 3, '2014-11-06 18:04:52'),
(2, 7, 3, '2014-11-06 18:05:10'),
(3, 8, 3, '2014-11-06 18:06:48'),
(4, 9, 3, '2014-11-06 18:09:54'),
(5, 10, 3, '2014-11-06 18:11:20'),
(6, 11, 3, '2014-11-06 18:13:15'),
(7, 12, 3, '2014-12-07 14:13:38'),
(8, 13, 3, '2014-12-07 14:22:50'),
(9, 15, 17, '2014-12-24 20:44:01'),
(10, 16, 17, '2014-12-24 20:50:53'),
(11, 17, 17, '2014-12-24 20:54:41'),
(12, 18, 17, '2014-12-24 20:57:16'),
(13, 19, 17, '2014-12-24 20:58:45'),
(14, 20, 17, '2014-12-24 21:01:00'),
(15, 21, 17, '2014-12-24 21:02:29'),
(16, 22, 17, '2014-12-24 21:03:37'),
(17, 23, 17, '2014-12-24 21:07:58'),
(18, 24, 17, '2014-12-24 21:09:02'),
(19, 25, 17, '2014-12-24 21:28:01'),
(20, 26, 17, '2014-12-24 21:39:25'),
(21, 27, 17, '2014-12-24 21:41:38'),
(22, 28, 17, '2014-12-24 21:42:29'),
(23, 29, 17, '2014-12-24 21:42:46'),
(24, 30, 17, '2014-12-24 21:42:48'),
(25, 31, 17, '2014-12-24 21:42:59'),
(26, 32, 27, '2014-12-27 09:27:56'),
(27, 33, 26, '2014-12-27 21:41:40'),
(28, 34, 26, '2014-12-27 21:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `subject_enrollments_users`
--
-- Creation: Dec 24, 2014 at 07:17 PM
-- Last update: Dec 28, 2014 at 03:12 PM
--

CREATE TABLE IF NOT EXISTS `subject_enrollments_users` (
`id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `progress` int(11) DEFAULT NULL,
  `time_enrolled` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `subject_enrollments_users`
--

INSERT INTO `subject_enrollments_users` (`id`, `subject_id`, `user_id`, `progress`, `time_enrolled`, `enrolled`, `updated`) VALUES
(1, 6, 3, 0, '2014-11-06 23:04:52', '2014-11-06 18:04:52', NULL),
(2, 7, 3, 0, '2014-11-06 23:05:10', '2014-11-06 18:05:10', NULL),
(3, 8, 3, 0, '2014-11-06 23:06:48', '2014-11-06 18:06:48', NULL),
(4, 9, 3, 0, '2014-11-06 23:09:54', '2014-11-06 18:09:54', NULL),
(5, 10, 3, 0, '2014-11-06 23:11:20', '2014-11-06 18:11:20', NULL),
(6, 11, 3, 0, '2014-11-06 23:13:15', '2014-11-06 18:13:15', NULL),
(7, 12, 3, 0, '2014-12-07 19:13:38', '2014-12-07 14:13:38', NULL),
(8, 13, 3, 0, '2014-12-07 19:22:50', '2014-12-07 14:22:50', NULL),
(9, 15, 17, 0, '2014-12-25 01:44:01', '2014-12-24 20:44:01', NULL),
(10, 16, 17, 0, '2014-12-25 01:50:53', '2014-12-24 20:50:53', NULL),
(11, 17, 17, 0, '2014-12-25 01:54:41', '2014-12-24 20:54:41', NULL),
(12, 18, 17, 0, '2014-12-25 01:57:16', '2014-12-24 20:57:16', NULL),
(13, 19, 17, 0, '2014-12-25 01:58:45', '2014-12-24 20:58:45', NULL),
(14, 20, 17, 0, '2014-12-25 02:01:00', '2014-12-24 21:01:00', NULL),
(15, 21, 17, 0, '2014-12-25 02:02:29', '2014-12-24 21:02:29', NULL),
(16, 22, 17, 0, '2014-12-25 02:03:37', '2014-12-24 21:03:37', NULL),
(17, 23, 17, 0, '2014-12-25 02:07:58', '2014-12-24 21:07:58', NULL),
(18, 24, 17, 0, '2014-12-25 02:09:02', '2014-12-24 21:09:02', NULL),
(19, 25, 17, 0, '2014-12-25 02:28:01', '2014-12-24 21:28:01', NULL),
(20, 26, 17, 0, '2014-12-25 02:39:25', '2014-12-24 21:39:25', NULL),
(21, 27, 17, 0, '2014-12-25 02:41:38', '2014-12-24 21:41:38', NULL),
(22, 28, 17, 0, '2014-12-25 02:42:29', '2014-12-24 21:42:29', NULL),
(23, 29, 17, 0, '2014-12-25 02:42:46', '2014-12-24 21:42:46', NULL),
(24, 30, 17, 0, '2014-12-25 02:42:48', '2014-12-24 21:42:48', NULL),
(25, 31, 17, 0, '2014-12-25 02:42:59', '2014-12-24 21:42:59', NULL),
(26, 32, 27, 0, '2014-12-27 14:27:56', '2014-12-27 09:27:56', NULL),
(27, 33, 26, 0, '2014-12-28 02:41:40', '2014-12-27 21:41:40', NULL),
(28, 34, 26, 0, '2014-12-28 02:42:22', '2014-12-27 21:42:22', NULL),
(29, 23, 26, 0, '2014-12-28 15:12:05', '2014-12-28 10:12:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--
-- Creation: Dec 24, 2014 at 07:17 PM
-- Last update: Dec 28, 2014 at 02:42 AM
--

CREATE TABLE IF NOT EXISTS `subjects` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `deck_order` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `description`, `creator_id`, `deck_order`, `created`, `updated`) VALUES
(1, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 17:57:18', '0000-00-00 00:00:00'),
(2, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 17:58:04', '0000-00-00 00:00:00'),
(3, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 17:59:33', '0000-00-00 00:00:00'),
(4, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 18:00:03', '0000-00-00 00:00:00'),
(5, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 18:02:22', '0000-00-00 00:00:00'),
(6, 'algebra 1', 'important subject. yeswow', 3, NULL, '2014-11-06 18:04:52', '0000-00-00 00:00:00'),
(7, 'algebra 1', 'important subject. yeswowwow', 3, NULL, '2014-11-06 18:05:10', '0000-00-00 00:00:00'),
(8, 'Chemistry 2 ', 'Learn how to make more meth.', 3, NULL, '2014-11-06 18:06:48', '0000-00-00 00:00:00'),
(9, 'Chemistry 3', 'Learn how to make even more meth..', 3, NULL, '2014-11-06 18:09:54', '0000-00-00 00:00:00'),
(10, 'Chemistry 4', 'Learn how to make lots more of meth..', 3, NULL, '2014-11-06 18:11:20', '0000-00-00 00:00:00'),
(11, 'Spanish', 'Learn how to read and write Spanish fluently.', 3, NULL, '2014-11-06 18:13:15', '0000-00-00 00:00:00'),
(12, 'SubjectMadebyAccount127', 'This is a SubjectMadebyAccount127', 3, NULL, '2014-12-07 14:13:38', '0000-00-00 00:00:00'),
(13, 'Microeconomics', 'Flashcards for MicroEcon 101', 3, NULL, '2014-12-07 14:22:50', '0000-00-00 00:00:00'),
(14, 'Account 128''s Trivia', 'Trivia Questions compiled by Account 128', NULL, NULL, '2014-12-24 20:40:27', '0000-00-00 00:00:00'),
(15, 'Account 128''s 2nd Trivia Subject', 'More Trivia compiled by account 128', 17, NULL, '2014-12-24 20:44:01', '0000-00-00 00:00:00'),
(16, 'Account128 3rd triv', 'more triv', 17, NULL, '2014-12-24 20:50:53', '0000-00-00 00:00:00'),
(17, 'Account 128''s 4th triv', 'yes more triv', 17, NULL, '2014-12-24 20:54:41', '0000-00-00 00:00:00'),
(18, 'Account 128''s 5th triv', 'yes indeedly more triv', 17, NULL, '2014-12-24 20:57:16', '0000-00-00 00:00:00'),
(19, 'Acc128 6triv', 'Acc128 6triv', 17, NULL, '2014-12-24 20:58:45', '0000-00-00 00:00:00'),
(20, 'acc128 triv7', 'acc128 triv7', 17, NULL, '2014-12-24 21:01:00', '0000-00-00 00:00:00'),
(21, 'acc128 triv8', 'acc128 triv8', 17, NULL, '2014-12-24 21:02:29', '0000-00-00 00:00:00'),
(22, 'Acc128 triv9', 'Acc128 triv9', 17, NULL, '2014-12-24 21:03:37', '0000-00-00 00:00:00'),
(23, 'acc128 10th triv', 'acc128 10th triv', 17, NULL, '2014-12-24 21:07:58', '0000-00-00 00:00:00'),
(24, 'Acc128 11triv', 'Acc128 11triv', 17, NULL, '2014-12-24 21:09:02', '0000-00-00 00:00:00'),
(25, 'acc128 12triv', 'acc 128 12triv', 17, NULL, '2014-12-24 21:28:01', '0000-00-00 00:00:00'),
(26, 'Acc138 13triv', 'Acc138 13triv', 17, NULL, '2014-12-24 21:39:25', '0000-00-00 00:00:00'),
(27, 'Acc128 14 triv?', 'Acc128 14 triv?', 17, NULL, '2014-12-24 21:41:38', '0000-00-00 00:00:00'),
(28, 'Acc128 15 triv', 'Acc128 15 triv', 17, NULL, '2014-12-24 21:42:29', '0000-00-00 00:00:00'),
(29, 'Acc128 15 triv', 'Acc128 15 triv', 17, NULL, '2014-12-24 21:42:46', '0000-00-00 00:00:00'),
(30, 'Acc128 15 triv', 'Acc128 15 triv', 17, NULL, '2014-12-24 21:42:48', '0000-00-00 00:00:00'),
(31, 'Acc128 16 triv', 'Acc128 16 triv', 17, NULL, '2014-12-24 21:42:59', '0000-00-00 00:00:00'),
(32, 'Account203sub', 'Account203subdesc', 27, NULL, '2014-12-27 09:27:56', '0000-00-00 00:00:00'),
(33, 'Account202''s first sub', 'Account202''s first sub', 26, NULL, '2014-12-27 21:41:40', '0000-00-00 00:00:00'),
(34, 'Account202''s 2nd sub', 'Account 202''s 2nd sub', 26, NULL, '2014-12-27 21:42:22', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `card_enrollments_editors`
--
ALTER TABLE `card_enrollments_editors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `card_enrollments_users`
--
ALTER TABLE `card_enrollments_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `card_interactions`
--
ALTER TABLE `card_interactions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `cards_in_decks`
--
ALTER TABLE `cards_in_decks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deck_enrollments_editors`
--
ALTER TABLE `deck_enrollments_editors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deck_enrollments_users`
--
ALTER TABLE `deck_enrollments_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decks`
--
ALTER TABLE `decks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decks_in_subjects`
--
ALTER TABLE `decks_in_subjects`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
 ADD KEY `user` (`user`(6)), ADD KEY `friend` (`friend`(6));

--
-- Indexes for table `subject_enrollments_editors`
--
ALTER TABLE `subject_enrollments_editors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_enrollments_users`
--
ALTER TABLE `subject_enrollments_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `card_enrollments_editors`
--
ALTER TABLE `card_enrollments_editors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `card_enrollments_users`
--
ALTER TABLE `card_enrollments_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `card_interactions`
--
ALTER TABLE `card_interactions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `cards_in_decks`
--
ALTER TABLE `cards_in_decks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deck_enrollments_editors`
--
ALTER TABLE `deck_enrollments_editors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `deck_enrollments_users`
--
ALTER TABLE `deck_enrollments_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `decks`
--
ALTER TABLE `decks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `decks_in_subjects`
--
ALTER TABLE `decks_in_subjects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `subject_enrollments_editors`
--
ALTER TABLE `subject_enrollments_editors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `subject_enrollments_users`
--
ALTER TABLE `subject_enrollments_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;--
-- Database: `intellidu_login`
--
CREATE DATABASE IF NOT EXISTS `intellidu_login` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `intellidu_login`;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--
-- Creation: Dec 24, 2014 at 07:17 PM
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `user_id`, `time`) VALUES
(1, 0, 0),
(2, 0, 0),
(3, 0, 0),
(4, 0, 0),
(5, 0, 0),
(6, 0, 0),
(7, 2, 1417213960),
(8, 2, 1417214020),
(9, 2, 1417214235),
(10, 2, 1417214349),
(11, 2, 1417215223),
(12, 6, 1417225178),
(13, 6, 1417226161),
(14, 6, 1417226220),
(15, 8, 1417226705),
(16, 3, 1419471556),
(17, 3, 1419471579),
(18, 25, 1419639799),
(19, 3, 1419691002),
(20, 3, 1419691054);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--
-- Creation: Dec 24, 2014 at 07:18 PM
--

CREATE TABLE IF NOT EXISTS `members` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(129) NOT NULL,
  `salt` varchar(129) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`) VALUES
(1, 'johnsimer', 'johnsimerlink@gmail.com', '1f04e2f9585d3ca7771a46a19ddbd73038d3818ccb8a0683bbed24c01b310def1e672b16a8f4f09bc330240101cf4d477c6b', '11d7dfdaebaddbfb46d7641b03684bbc2393e316e8b3688ca13744e9674b8286d69a7ae319f648f74d3c3461ac62c650c243'),
(2, 'Account1', 'Account1@gmail.com', 'f26fb559c4523e499d133cc173909766053e778f6be68dbf6d2af6d370447296a2bd4e6b399e3476e540b6d90efaa57ddc73', 'f22a05337edc3c02b410e681e160cd70cc047472afffac63a4c117ef2e9685a69b2eeeed5cf0a00e1ee514f988ec8ab7fd3b'),
(3, 'Account2', 'Account2@gmail.com', 'fac4001d54648107f07eeec92cf0451d3123bc9dccbdb72b5d5d724cafcd4584aeac9e84b62cdf015887812ce3ca7728052e', '4eacf488268499ff23ba19400f5a2c111a390cba425555af89cfa8c082c639d59221a8d4928043af93795d5548a123b44bd8'),
(4, 'Account3', 'Account3@gmail.com', '7ea95614f2c71fc3313244c137fc30c672325bbe995c337b11b7cf85dbd1836df4d7b18301fae15eda0a5057d6dd4fbd7dca', '7fb5493893e65748c95cd7209c15dbbd0e9b3f69242f054b184a4247e2157973fd56f0b8865b52d554c76bf2f9663500800d'),
(5, 'Account4', 'Account4@gmail.com', '3c92a578d7ef1ab549eae2533b80d3045252e650c8de3a8d60277d036d4798bb1611e2b9c4a9c6bf5d348182b042f97e8a84', 'd6c9739462ffafdcfb40b9ee676c0528bb48be17709d9fb79d40e0a3b22723fb754daf5a6375ba80c3ad66ed39c612cefc1c'),
(6, 'Account5', 'Account5@gmail.com', '21b098d15e81533497b4d65d0b6d17aebfa07cb0c0e926f08409e8804ba298c01b88eb1523e329b4596ce03cf6cc61dea801', '07893ef78c96e7c62c378a6928e32e51aa1591604efad77b3ac59d653e8b0a34d2c5691cab860c66cab75de29dae4a425dfb'),
(7, 'Account6', 'Account6@gmail.com', '5079b5b4b734d3ddf9804acafc47e69907edfb412c583a65139804c0f5a512633060ab287a7481ecaf4e69bc94f88b3ed1cd', '7e8f9211f17fcf037130699b56f4eaba63d566f0f4a626da81f656c1059530664870ca5f325ef9a58176cc9bda79f2f55780'),
(8, 'Account7', 'Account7@gmail.com', '6fed908d01c73c49332b0876343fb8dc90ca8c8e5344ab52335de5a2d4e70cdab6211321f47d8bd24594e539f17f034f41db', '0005c95876463b389ff7872bd801ee862ff2d67ba313812a1c30fb2bc153bb2befd8599b6cb0456d3dde52fbf1aee6cae50c'),
(9, 'Account8', 'Account8@gmail.com', 'b0efc5b88ec8b043fb8f1ac16bd9b9fb7e9f8f5d7c02b2c4750cddd431b7899266667b7e7371ef3fe3eda3063a1157794a9503c536e0f25157d38f6dc1b116ce', '229009eccdb85ee3dc102f934b190b58ab63d77116b002d9ade13ffea10c44fc5c339d5e2b10a45bc131f87d615ab4e065f576d71e507fced1b573cd19986964'),
(10, 'Account9', 'Account9@gmail.com', 'a9d3b6e4cbe6814196b3bce298ee74cc6dc75d40efe75f556d93efd0c5c4be27d542d42013eb46c65811ec77c5715e3837c9f07b008a0988a3ccda0c70aab0bf', 'e4043cc7226e6c66da767d23d8bd090afe56526825e3c56a4d15825883eb7cfe4bfc4cdeece2fd71ece1b9e66fd3d92473209ea556767c161c552efec5c09f92'),
(11, 'Account10', 'Account10@gmail.com', '9919e5ce15b6f903fb85503dc1ff1e0d35864439066dc5e69e3a606ede63a1da3f97da63b956703012ef78fec13e2b331eb98cb187b78f27eef3949688da776b', '5e0951336209f47a430796ca658c103856800187467f8d305fa1037466cce1ddd1d19273582c0fb9d32b7aa72d2f444f3f1e1235073d7b3913c0b5487ab9eedf'),
(12, 'Account12', 'Account12@gmail.com', '55def68cabe6e4ca5cacd7b131c24c5498824ccc1e473049fcaea09318f92d4ee1533798cd50d41b08763aafae27a3f597db5fdad2da715b2f939d90e6e8fd1f', '79149c35ddc0aceb2ad73ac1e182d07823ecea1193cf5bc897b2a034cca04d78bf3f6d4c7930b1f6f33176514de4b4000722fbe1333578981c62efb55944e0a9'),
(13, 'Account13', 'Account13@gmail.com', 'd142b8de681cb15a26cde6fbb9e79828df7076471218826c41ae4f0733184f33027b3af28ba94ab368556310dd7d0f91d43224e06091d95f81f95031dca19b03', '33eca7d3314b0ebb7ce1e48414adcbf4fc501499fd3cf4f1e551ca73c80894e5dc4d1b5e8d3bff41233754dc5a42e54e63be3ef79ad34bf3f73cc98b52200060'),
(14, 'Account15', 'Account15@gmail.com', '349b5caca4df0de6f9d2d4982351c19f5e2ce79c8bc61f1997fe839df045e6b772943d77f0fe9e6c1ab4586ce866c1ec9c9a8bd8571d1f66f097e91e0a4b09a4', '824dffe672c469b8759e6cb876ec1ad4af8a9508787f404f17ba5eeaee0fe5d473f0650727008d5fc0942e9cc124ded6cbcb9a4d43a1b9ec40ac51f63d77967f'),
(15, 'Account16', 'Account16@gmail.com', '26e8a900438ffb68ae577cd98a55318c93e3f447cda25c2fe4256932562678816583d36aeebeb2ce2f34b63eeb4d1d9d2aa09a49f6a4eacdf2b3c6eed7bb3c9d', '53a533db74bac37f12c4372b555249969e0d275805e607ff1961c244633b1816a624800376e88cf6a50237ed35cea0f9b29f0731a7396071c5d784c91abc6d07'),
(16, 'Account127', 'Account127@gmail.com', 'b1e9df78d121f7b039626a863294e3db464b08517168d97867902f487e561d0cbbdbf2f08437f076948ec0c6f9971070758d8a46abae80f247e08ea1d2d1c3a0', '4b2b928be86d1ffd6a039e6cd2719624acbc9e8e24a2f498afe7194adbc57ea22965936128692e3925181c62bd7980fdc998360a8fea4069d4754f6ed90395ed'),
(17, 'Account128', 'Account128@gmail.com', '02f9c2c35db4bf6047482b2443f9ea61ed72b4ec94d30dae55c439b30fe93233cfa7ebd7e234984a479c649b12d31bc0da97af221bf1bf0548bc813ac2976647', '678f169e2df29127713216f2f80e33cb20ba327fecc4a43e280cca92c7a7767c13c1fafe75c61f58381443770415b49d05f593d6c5e9057f0134df31bf27152e'),
(18, 'Johneve1', 'Johneve1@gmail.com', '14c7c6696dba770f6d9d3d2bac5b7da91230947b56bf2ba305950c90e42732c7786502f420e048a246836ff7665a585b72b20fc3af407166aa83de1451d4e87a', '94a48a7332ef127c456da87a1145af8c6fa48e73a2ea469090083802afb6c93c16bf26f86616c9fa601e1dc1d016c346b5dbe7bbe75a1338e9133b664322616c'),
(19, 'Johneve2', 'Johneve2@gmail.com', '84f2fb7372b2c61f5f8c572df5840388c817d786e355155cf4928077aa0919759876ec77546781b79a811d9a303ec4de07089719abd0e29539461ffd0d45ac1c', 'fa8f4b99cc700927729c0742476b2b1c07ef41000357143ca3e3ccc357b86b18225153f6954dcf660a61e1702292558957c3f34a0530771dc0843e967cd58c1f'),
(20, 'Johneve3', 'Johneve3@gmail.com', '4649787855449d861e547b8fb0046e7663d16e1c61a5b5979497b045a987d546f72f1d1643f9f5830068700f55254c7cb825956685645e20032dda40ff13782a', 'e294ab493ec3422b148c1dad7a62d1a94baddd722f9610ffe6a5ebcac0421e2cc93f3379fe5f276d07d9ea9c2f3172d950af99846a7c1723d535e0dcb2212ddf'),
(21, 'Johneve4', 'Johneve4@gmail.com', '32f9fe619f5f969f71a1e894378462f58f0bb514f5465538b54d6d0675df3714fba60523b1c7d7fcb706a07e23b5ea6e4111c5e173c6619e518efdee0d85359c', '48ede11e57d36b8b8e6cbc32fac525bdde97a9166259c355dfd3bc138e1e5a9dcf26b9a8b53e27d6be7fb6320c03494df302d5252070cbc080132cf4d2b2323c'),
(22, 'Johneve5', 'Johneve5@gmail.com', 'dcc0b6ee9df15c5b85cf005069ab41c1a7c779a6cc6e5757c797dd84841f45701d1ae30fbb721f8e55683881f86a04a09c5310cfef68f46a155f1f37c85a4f0b', '93f848aee37446f27a7cf5ae5f2ee8aedd2331b6f829b1f5d14b1d491440c11fabb9fb3b2cf30a424f3867c8fb09f3f492b6914477255ec8860fb67b14ca3a77'),
(23, 'Johneve7', 'Johneve7@gmail.com', 'b195b46b8bf9696a788245fccd8ebeccaf490c2453893bb4bcdf9416116671a52b12617dd3810021e7f9a4f970ac75e444fc356acd3f06d9f6ec05e67af2535e', '7778dcffbfdcf7115f6f75fc72e8f9382b7a494b9751b9966df76d12f6695941b4524223f9b79a60a2d840d5b6ba980a8dde96e0f4368100a6864092fd0396ed'),
(24, 'Johneve8', 'Johneve8@gmail.com', '6f1bc336f1bc781bbf153f15453a43dc20a74c936ade0ac3a2b0079267b8a034c5c6c1196006448bae596b6ecefe5dc079d441332740580b2d786c647ad30bfb', '944e533009dc13b6844617afc727f9607489e2e4717e9411aa329b269e42f77a0896e7b9b263748e41a6f5eb8f7a312993cee934a7bff44574a3eaa84510cbc8'),
(25, 'Account201', 'Account201@gmail.com', '460a8c51aa52beadfb54f6ee38318a09fce13ee8965b01d5e2cab417ef84b15d816a2f1493fa30eaa9cadf7d6d67f3d661de5c338e0fb3d67ab974f12ffe814a', '1e49fbdba56262afd715e95876b93a5a78452dc5cc83947542a5f764b1d071708240a9722afbb3fb091928c0d9788a9bb8eb5f42d12abfb9dcada6c2db38d9cf'),
(26, 'Account202', 'Account202@gmail.com', 'b2f758b9e2b3e30c8466aa1e779956b9c2a732b834500a33449c03440387a89fff0f2bfbc5588bb2cd9ee75597d83e5367e4e2bc0ce3da5ded9be4560d7dc0f3', '686030a8dd77cff77c5ec74aae8aca15015c00457e24cc2a95297d48b70f638bef8697ba7e75aa89e5341822b6dd57024f4953482a752851b4b7c403e2ce637d'),
(27, 'Account203', 'Account203@gmail.com', 'fecdcae34fd06d53491f29bf1b2c6eea3298c22208ee501690be7243115004117bd7efadfbc5ea58e5ceecc9845573b12761d0d2f0af3c960927fa4bc658e6a4', 'd183fdfe3155f80508815c86ccaeb8918c4194b9e70a23c89d33b80836ef4460893d71949684b08d34906ca01744504240b3b569e43c6e01ff9dc35159e7331f'),
(28, 'Account204', 'Account204@gmail.com', '4a2b16f025eb38b3f362e3664cc6ec0d253e32727a9350e0117ec91824ea8cdc6af5df1e3b4b1ac20bd841d35ff64a9a93d0ef701e85882406713cc052ec4860', 'c2a53c24f875c2adc0833a401821640e42bf0df0160d8c46b76c1a021f1a7280de023d398de901057f94b8399fb5e5cf125b90754ac112b8a0f582bfc47b5764'),
(29, 'Account205', 'Account205@gmail.com', '20d4b76b5d5e414fa49cd9835c3fba3e611b941be097f4102d0c9f6ea15f028e9aa362d8de1f7ebe22887b33cbea94ea4656fd77d61d3255a4178e75f679e0fd', '329c0a624a2dcdac27befb2775a0994f938a034495e5af6a909c4b9162f3614cd3242d4b91191b4b630cca38cc197d700ebd2f79f79b1c4f3116bce8f2292e95');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
