-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2014 at 08:05 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `intellidu_data`
--
CREATE DATABASE IF NOT EXISTS `intellidu_data` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `intellidu_data`;

-- --------------------------------------------------------

--
-- Table structure for table `card_enrollments_editors`
--

CREATE TABLE IF NOT EXISTS `card_enrollments_editors` (
`id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `editor_id` int(11) NOT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `card_enrollments_editors`
--

INSERT INTO `card_enrollments_editors` (`id`, `card_id`, `editor_id`, `enrolled`) VALUES
(1, 1, 3, '2014-11-06 18:14:09'),
(2, 2, 3, '2014-11-06 18:14:14'),
(3, 3, 3, '2014-11-06 18:14:19'),
(4, 4, 3, '2014-11-06 18:14:23'),
(5, 6, 3, '2014-11-22 09:55:53'),
(6, 7, 3, '2014-11-22 09:56:07'),
(7, 9, 3, '2014-11-22 10:31:58'),
(8, 10, 3, '2014-11-22 10:34:01'),
(9, 11, 3, '2014-11-22 10:34:11'),
(10, 12, 3, '2014-11-22 10:34:19'),
(11, 13, 3, '2014-11-22 10:34:27'),
(12, 15, 3, '2014-11-30 13:10:23'),
(13, 16, 3, '2014-11-30 13:10:33'),
(14, 17, 3, '2014-11-30 13:10:38'),
(15, 18, 3, '2014-11-30 13:10:42'),
(16, 20, 3, '2014-12-07 14:15:43'),
(17, 21, 3, '2014-12-19 14:03:01'),
(18, 22, 3, '2014-12-19 14:03:07'),
(19, 23, 3, '2014-12-19 14:03:19');

-- --------------------------------------------------------

--
-- Table structure for table `card_enrollments_users`
--

CREATE TABLE IF NOT EXISTS `card_enrollments_users` (
`id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `progress` int(11) DEFAULT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `confidence` int(11) DEFAULT NULL,
  `next_review_time` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `card_enrollments_users`
--

INSERT INTO `card_enrollments_users` (`id`, `card_id`, `user_id`, `progress`, `enrolled`, `updated`, `confidence`, `next_review_time`) VALUES
(1, 1, 3, NULL, '2014-11-06 18:14:09', NULL, NULL, NULL),
(2, 2, 3, NULL, '2014-11-06 18:14:14', NULL, NULL, NULL),
(3, 3, 3, NULL, '2014-11-06 18:14:18', NULL, NULL, NULL),
(4, 4, 3, NULL, '2014-11-06 18:14:23', NULL, NULL, NULL),
(5, 6, 3, NULL, '2014-11-22 09:55:53', NULL, NULL, NULL),
(6, 7, 3, NULL, '2014-11-22 09:56:07', '2014-12-19 14:04:12', 5, '2014-12-23 03:06:12'),
(7, 9, 3, NULL, '2014-11-22 10:31:58', '2014-12-19 14:29:00', 5, '2014-12-23 03:30:56'),
(8, 10, 3, NULL, '2014-11-22 10:34:01', '2014-12-19 14:04:02', 5, '2014-12-23 03:06:02'),
(9, 11, 3, NULL, '2014-11-22 10:34:11', '2014-12-19 14:04:04', 5, '2014-12-23 03:06:04'),
(10, 12, 3, NULL, '2014-11-22 10:34:19', '2014-12-19 14:04:06', 5, '2014-12-23 03:06:06'),
(11, 13, 3, NULL, '2014-11-22 10:34:27', '2014-12-19 14:04:08', 5, '2014-12-23 03:06:08'),
(12, 15, 3, NULL, '2014-11-30 13:10:23', '2014-12-19 14:04:15', 5, '2014-12-26 11:08:15'),
(13, 16, 3, NULL, '2014-11-30 13:10:33', '2014-12-19 14:04:17', 5, '2014-12-26 11:08:17'),
(14, 17, 3, NULL, '2014-11-30 13:10:38', '2014-12-19 14:04:19', 5, '2014-12-26 11:08:18'),
(15, 18, 3, NULL, '2014-11-30 13:10:42', '2014-12-19 14:04:13', 5, '2014-12-23 03:06:13'),
(16, 20, 3, NULL, '2014-12-07 14:15:43', NULL, NULL, NULL),
(17, 21, 3, NULL, '2014-12-19 14:03:00', '2014-12-19 14:29:18', 5, '2014-12-26 11:33:17'),
(18, 22, 3, NULL, '2014-12-19 14:03:06', '2014-12-19 14:29:20', 5, '2014-12-26 11:33:19'),
(19, 23, 3, NULL, '2014-12-19 14:03:18', '2014-12-19 14:29:22', 5, '2014-12-26 11:33:22');

-- --------------------------------------------------------

--
-- Table structure for table `card_interactions`
--

CREATE TABLE IF NOT EXISTS `card_interactions` (
`id` int(11) NOT NULL,
  `card_enrollment_id` int(11) DEFAULT NULL,
  `interaction_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `confidence` int(11) DEFAULT NULL,
  `time_front` int(11) DEFAULT NULL,
  `time_back` int(11) DEFAULT NULL,
  `number_of_flips` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `card_interactions`
--

INSERT INTO `card_interactions` (`id`, `card_enrollment_id`, `interaction_time`, `confidence`, `time_front`, `time_back`, `number_of_flips`) VALUES
(1, 6, '2014-11-28 22:59:40', 1, 6269, 1599, 1),
(2, 6, '2014-11-28 23:02:39', 2, 81746, 3307, 1),
(3, 7, '2014-11-28 23:03:07', 5, 22948, 3252, 1),
(4, 8, '2014-11-28 23:03:14', 5, 5136, 1209, 1),
(5, 9, '2014-11-28 23:03:20', 5, 2648, 1401, 1),
(6, 10, '2014-11-28 23:03:28', 5, 6177, 1291, 1),
(7, 11, '2014-11-28 23:03:33', 5, 2620, 992, 1),
(8, 6, '2014-11-29 21:25:58', 1, 2437, 1328, 1),
(9, 6, '2014-11-29 21:26:41', 1, 3956, 1235, 1),
(10, 6, '2014-11-30 13:15:15', 1, 117717, 2816, 3),
(11, 12, '2014-11-30 13:15:17', 1, 1340, 800, 1),
(12, 13, '2014-11-30 13:15:19', 1, 531, 969, 1),
(13, 14, '2014-11-30 13:15:21', 1, 707, 1228, 1),
(14, 15, '2014-11-30 13:18:55', 1, 211782, 2072, 3),
(15, 6, '2014-11-30 13:19:48', 5, 12156, 1045, 1),
(16, 12, '2014-11-30 13:19:50', 5, 585, 876, 1),
(17, 13, '2014-11-30 13:19:51', 5, 529, 928, 1),
(18, 14, '2014-11-30 13:19:53', 5, 546, 617, 1),
(19, 15, '2014-11-30 13:19:54', 5, 544, 878, 1),
(20, 12, '2014-11-30 13:20:01', 5, 2752, 1076, 1),
(21, 13, '2014-11-30 13:20:03', 5, 582, 643, 1),
(22, 14, '2014-11-30 13:20:04', 5, 532, 669, 1),
(23, 7, '2014-12-19 14:04:01', 2, 8107, 7703, 2),
(24, 8, '2014-12-19 14:04:03', 5, 3361, 2235, 1),
(25, 9, '2014-12-19 14:04:05', 5, 1144, 757, 1),
(26, 10, '2014-12-19 14:04:07', 5, 1056, 1237, 1),
(27, 11, '2014-12-19 14:04:09', 5, 790, 881, 1),
(28, 6, '2014-12-19 14:04:13', 5, 2865, 1010, 1),
(29, 15, '2014-12-19 14:04:14', 5, 713, 762, 1),
(30, 12, '2014-12-19 14:04:16', 5, 577, 1390, 1),
(31, 13, '2014-12-19 14:04:20', 5, 786, 604, 1),
(32, 14, '2014-12-19 14:04:20', 5, 595, 1262, 1),
(33, 17, '2014-12-19 14:04:23', 5, 1058, 2430, 1),
(34, 18, '2014-12-19 14:04:26', 5, 932, 1294, 1),
(35, 19, '2014-12-19 14:04:27', 5, 1011, 973, 1),
(36, 7, '2014-12-19 14:28:59', 5, 4776, 1764, 1),
(37, 17, '2014-12-19 14:29:07', 5, 4058, 4639, 1),
(38, 18, '2014-12-19 14:29:08', 5, 993, 795, 1),
(39, 19, '2014-12-19 14:29:09', 5, 913, 801, 1),
(40, 17, '2014-12-19 14:29:18', 5, 3977, 1332, 1),
(41, 18, '2014-12-19 14:29:20', 5, 1027, 864, 1),
(42, 19, '2014-12-19 14:29:22', 5, 1012, 1154, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE IF NOT EXISTS `cards` (
`id` int(11) NOT NULL,
  `front` varchar(1000) COLLATE latin1_general_ci DEFAULT NULL,
  `back` varchar(1000) COLLATE latin1_general_ci DEFAULT NULL,
  `skill` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `skill_group` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `subskill` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `front`, `back`, `skill`, `skill_group`, `subskill`, `created`, `updated`) VALUES
(1, 'amigo', 'friend ', '', NULL, NULL, '2014-11-06 18:14:09', '2014-11-06 18:14:26'),
(2, 'dorito', 'dorito ', '', NULL, NULL, '2014-11-06 18:14:14', '2014-11-06 18:14:26'),
(3, 'taco', 'taco ', '', NULL, NULL, '2014-11-06 18:14:18', '2014-11-06 18:14:26'),
(4, '', '', '', NULL, NULL, '2014-11-06 18:14:23', '0000-00-00 00:00:00'),
(5, 'hola', 'hello ', '', NULL, NULL, '2014-11-06 18:14:26', '0000-00-00 00:00:00'),
(6, 'Card 2 Front', 'Card 2 back ', '', NULL, NULL, '2014-11-22 09:55:53', '2014-11-22 09:56:52'),
(7, 'Why does meth explode?', 'methane ', '', NULL, NULL, '2014-11-22 09:56:06', '2014-11-22 10:36:08'),
(8, 'Card 1 front', 'card 1 back  ', '', NULL, NULL, '2014-11-22 09:56:52', '0000-00-00 00:00:00'),
(9, 'How can I make meth', 'watch breaking bad ', '', NULL, NULL, '2014-11-22 10:31:57', '2014-11-22 10:36:08'),
(10, 'What color is meth', 'white ', '', NULL, NULL, '2014-11-22 10:34:01', '2014-11-22 10:36:08'),
(11, 'what color is cool meth', 'blue ', '', NULL, NULL, '2014-11-22 10:34:11', '2014-11-22 10:36:08'),
(12, '', '', '', NULL, NULL, '2014-11-22 10:34:19', '0000-00-00 00:00:00'),
(13, '', '', '', NULL, NULL, '2014-11-22 10:34:27', '0000-00-00 00:00:00'),
(14, 'Card 1 front', 'card 1 back   ', '', NULL, NULL, '2014-11-22 10:36:08', '0000-00-00 00:00:00'),
(15, 'card 2', 'card 2 back ', '', NULL, NULL, '2014-11-30 13:10:23', '2014-11-30 13:10:49'),
(16, 'card 3', 'card 3 back ', '', NULL, NULL, '2014-11-30 13:10:33', '2014-11-30 13:10:49'),
(17, 'card 4 ', 'card 4 back ', '', NULL, NULL, '2014-11-30 13:10:38', '2014-11-30 13:10:49'),
(18, '', '', '', NULL, NULL, '2014-11-30 13:10:42', '0000-00-00 00:00:00'),
(19, 'Card 1', 'card 1 back ', '', NULL, NULL, '2014-11-30 13:10:49', '0000-00-00 00:00:00'),
(20, '', '', '', NULL, NULL, '2014-12-07 14:15:43', '0000-00-00 00:00:00'),
(21, 'Step 2', 'duggie ', '', NULL, NULL, '2014-12-19 14:02:57', '2014-12-19 14:03:23'),
(22, 'step 3', 'Be amazed. ', '', NULL, NULL, '2014-12-19 14:03:06', '2014-12-19 14:03:23'),
(23, '', '', '', NULL, NULL, '2014-12-19 14:03:16', '0000-00-00 00:00:00'),
(24, 'Step 1', 'Get your dugg on ', '', NULL, NULL, '2014-12-19 14:03:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cards_in_decks`
--

CREATE TABLE IF NOT EXISTS `cards_in_decks` (
`id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `order_in_deck` int(11) NOT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `cards_in_decks`
--

INSERT INTO `cards_in_decks` (`id`, `card_id`, `deck_id`, `order_in_deck`, `enrolled`) VALUES
(1, 1, 1, 0, '2014-11-06 18:14:09'),
(2, 2, 1, 0, '2014-11-06 18:14:14'),
(3, 3, 1, 0, '2014-11-06 18:14:18'),
(4, 4, 1, 0, '2014-11-06 18:14:23'),
(6, 7, 2, 0, '2014-11-22 09:56:06'),
(7, 9, 2, 0, '2014-11-22 10:31:57'),
(8, 10, 2, 0, '2014-11-22 10:34:01'),
(9, 11, 2, 0, '2014-11-22 10:34:11'),
(10, 12, 2, 0, '2014-11-22 10:34:19'),
(11, 13, 2, 0, '2014-11-22 10:34:27'),
(12, 15, 7, 0, '2014-11-30 13:10:23'),
(13, 16, 7, 0, '2014-11-30 13:10:33'),
(14, 17, 7, 0, '2014-11-30 13:10:38'),
(15, 18, 7, 0, '2014-11-30 13:10:42'),
(16, 20, 8, 0, '2014-12-07 14:15:43'),
(17, 21, 14, 0, '2014-12-19 14:02:58'),
(18, 22, 14, 0, '2014-12-19 14:03:06'),
(19, 23, 14, 0, '2014-12-19 14:03:16');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `deck_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `subject_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deck_enrollments_editors`
--

CREATE TABLE IF NOT EXISTS `deck_enrollments_editors` (
`id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `editor_id` int(11) NOT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `deck_enrollments_editors`
--

INSERT INTO `deck_enrollments_editors` (`id`, `deck_id`, `editor_id`, `enrolled`) VALUES
(1, 1, 3, '2014-11-06 18:13:50'),
(2, 2, 3, '2014-11-22 09:20:12'),
(3, 3, 3, '2014-11-22 09:20:15'),
(4, 4, 3, '2014-11-22 09:25:47'),
(5, 5, 3, '2014-11-22 09:26:15'),
(6, 6, 3, '2014-11-22 09:54:36'),
(7, 7, 3, '2014-11-30 13:08:25'),
(8, 8, 3, '2014-12-07 14:15:18'),
(9, 9, 3, '2014-12-07 14:16:01'),
(10, 10, 3, '2014-12-07 14:16:11'),
(11, 11, 3, '2014-12-07 14:16:18'),
(12, 12, 3, '2014-12-07 14:20:37'),
(13, 13, 3, '2014-12-07 14:21:22'),
(14, 14, 3, '2014-12-19 14:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `deck_enrollments_users`
--

CREATE TABLE IF NOT EXISTS `deck_enrollments_users` (
`id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `progress` int(11) DEFAULT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `deck_enrollments_users`
--

INSERT INTO `deck_enrollments_users` (`id`, `deck_id`, `user_id`, `progress`, `enrolled`, `updated`) VALUES
(1, 1, 3, 0, '2014-11-06 18:13:50', NULL),
(2, 2, 3, 0, '2014-11-22 09:20:12', NULL),
(3, 3, 3, 0, '2014-11-22 09:20:15', NULL),
(4, 4, 3, 0, '2014-11-22 09:25:47', NULL),
(5, 5, 3, 0, '2014-11-22 09:26:15', NULL),
(6, 6, 3, 0, '2014-11-22 09:54:36', NULL),
(7, 7, 3, 0, '2014-11-30 13:08:25', NULL),
(8, 8, 3, 0, '2014-12-07 14:15:18', NULL),
(9, 9, 3, 0, '2014-12-07 14:16:01', NULL),
(10, 10, 3, 0, '2014-12-07 14:16:11', NULL),
(11, 11, 3, 0, '2014-12-07 14:16:18', NULL),
(12, 12, 3, 0, '2014-12-07 14:20:37', NULL),
(13, 13, 3, 0, '2014-12-07 14:21:22', NULL),
(14, 14, 3, 0, '2014-12-19 14:02:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `decks`
--

CREATE TABLE IF NOT EXISTS `decks` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `creator_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `decks`
--

INSERT INTO `decks` (`id`, `name`, `description`, `creator_id`, `created`, `updated`) VALUES
(1, 'Pronunciation', 'Learn the pronunciation of the basic Spanish words.', '3', '2014-11-06 18:13:50', '0000-00-00 00:00:00'),
(2, 'Introduction to Meth Making', 'Learn the basic of making meth', '3', '2014-11-22 09:20:11', '0000-00-00 00:00:00'),
(3, 'Introduction to Meth Making', 'Learn the basic of making meth', '3', '2014-11-22 09:20:15', '0000-00-00 00:00:00'),
(4, 'Sample Deck', 'sample description', '3', '2014-11-22 09:25:47', '0000-00-00 00:00:00'),
(5, 'sample deck  2', 'sample description 2', '3', '2014-11-22 09:26:14', '0000-00-00 00:00:00'),
(6, 'Meth Waars', 'Learn about the wars surrounding meth', '3', '2014-11-22 09:54:36', '0000-00-00 00:00:00'),
(7, 'This deck has cards', 'This deck has cards', '3', '2014-11-30 13:08:25', '0000-00-00 00:00:00'),
(8, 'X+5 = 10', 'First deck in algebra1', '3', '2014-12-07 14:15:18', '0000-00-00 00:00:00'),
(9, 'Deck2', 'this is the second deck in algebra  1', '3', '2014-12-07 14:16:01', '0000-00-00 00:00:00'),
(10, 'Deck3', 'this is the third deck in algebra  1', '3', '2014-12-07 14:16:11', '0000-00-00 00:00:00'),
(11, 'Deck4', 'this is the fourth deck in algebra  1', '3', '2014-12-07 14:16:18', '0000-00-00 00:00:00'),
(12, 'Algebra 1 first deck', 'first deck', '3', '2014-12-07 14:20:37', '0000-00-00 00:00:00'),
(13, 'Algebra 2 first deck', 'second deck', '3', '2014-12-07 14:21:22', '0000-00-00 00:00:00'),
(14, 'How to duggie', 'This deck will teach you how to duggie - flashcard style', '3', '2014-12-19 14:02:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `decks_in_subjects`
--

CREATE TABLE IF NOT EXISTS `decks_in_subjects` (
`id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `decks_in_subjects`
--

INSERT INTO `decks_in_subjects` (`id`, `subject_id`, `deck_id`, `enrolled`) VALUES
(1, 11, 1, '2014-11-06 18:13:50'),
(2, 8, 2, '2014-11-22 09:20:12'),
(3, 8, 3, '2014-11-22 09:20:15'),
(4, 8, 4, '2014-11-22 09:25:47'),
(5, 8, 5, '2014-11-22 09:26:14'),
(6, 8, 6, '2014-11-22 09:54:36'),
(7, 8, 7, '2014-11-30 13:08:25'),
(8, 6, 8, '2014-12-07 14:15:18'),
(9, 6, 9, '2014-12-07 14:16:01'),
(10, 6, 10, '2014-12-07 14:16:11'),
(11, 6, 11, '2014-12-07 14:16:18'),
(12, 7, 12, '2014-12-07 14:20:37'),
(13, 7, 13, '2014-12-07 14:21:22'),
(14, 12, 14, '2014-12-19 14:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `user` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `friend` varchar(16) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subject_enrollments_editors`
--

CREATE TABLE IF NOT EXISTS `subject_enrollments_editors` (
`id` int(11) NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `subject_enrollments_editors`
--

INSERT INTO `subject_enrollments_editors` (`id`, `subject_id`, `editor_id`, `enrolled`) VALUES
(1, NULL, 3, '2014-11-06 18:04:52'),
(2, 7, 3, '2014-11-06 18:05:10'),
(3, 8, 3, '2014-11-06 18:06:48'),
(4, 9, 3, '2014-11-06 18:09:54'),
(5, 10, 3, '2014-11-06 18:11:20'),
(6, 11, 3, '2014-11-06 18:13:15'),
(7, 12, 3, '2014-12-07 14:13:38'),
(8, 13, 3, '2014-12-07 14:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `subject_enrollments_users`
--

CREATE TABLE IF NOT EXISTS `subject_enrollments_users` (
`id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `progress` int(11) DEFAULT NULL,
  `time_enrolled` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enrolled` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `subject_enrollments_users`
--

INSERT INTO `subject_enrollments_users` (`id`, `subject_id`, `user_id`, `progress`, `time_enrolled`, `enrolled`, `updated`) VALUES
(1, 6, 3, 0, '2014-11-06 23:04:52', '2014-11-06 18:04:52', NULL),
(2, 7, 3, 0, '2014-11-06 23:05:10', '2014-11-06 18:05:10', NULL),
(3, 8, 3, 0, '2014-11-06 23:06:48', '2014-11-06 18:06:48', NULL),
(4, 9, 3, 0, '2014-11-06 23:09:54', '2014-11-06 18:09:54', NULL),
(5, 10, 3, 0, '2014-11-06 23:11:20', '2014-11-06 18:11:20', NULL),
(6, 11, 3, 0, '2014-11-06 23:13:15', '2014-11-06 18:13:15', NULL),
(7, 12, 3, 0, '2014-12-07 19:13:38', '2014-12-07 14:13:38', NULL),
(8, 13, 3, 0, '2014-12-07 19:22:50', '2014-12-07 14:22:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `deck_order` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `description`, `creator_id`, `deck_order`, `created`, `updated`) VALUES
(1, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 17:57:18', '0000-00-00 00:00:00'),
(2, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 17:58:04', '0000-00-00 00:00:00'),
(3, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 17:59:33', '0000-00-00 00:00:00'),
(4, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 18:00:03', '0000-00-00 00:00:00'),
(5, 'algebra 1', 'important subject', 3, NULL, '2014-11-06 18:02:22', '0000-00-00 00:00:00'),
(6, 'algebra 1', 'important subject. yeswow', 3, NULL, '2014-11-06 18:04:52', '0000-00-00 00:00:00'),
(7, 'algebra 1', 'important subject. yeswowwow', 3, NULL, '2014-11-06 18:05:10', '0000-00-00 00:00:00'),
(8, 'Chemistry 2 ', 'Learn how to make more meth.', 3, NULL, '2014-11-06 18:06:48', '0000-00-00 00:00:00'),
(9, 'Chemistry 3', 'Learn how to make even more meth..', 3, NULL, '2014-11-06 18:09:54', '0000-00-00 00:00:00'),
(10, 'Chemistry 4', 'Learn how to make lots more of meth..', 3, NULL, '2014-11-06 18:11:20', '0000-00-00 00:00:00'),
(11, 'Spanish', 'Learn how to read and write Spanish fluently.', 3, NULL, '2014-11-06 18:13:15', '0000-00-00 00:00:00'),
(12, 'SubjectMadebyAccount127', 'This is a SubjectMadebyAccount127', 3, NULL, '2014-12-07 14:13:38', '0000-00-00 00:00:00'),
(13, 'Microeconomics', 'Flashcards for MicroEcon 101', 3, NULL, '2014-12-07 14:22:50', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `card_enrollments_editors`
--
ALTER TABLE `card_enrollments_editors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `card_enrollments_users`
--
ALTER TABLE `card_enrollments_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `card_interactions`
--
ALTER TABLE `card_interactions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `cards_in_decks`
--
ALTER TABLE `cards_in_decks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deck_enrollments_editors`
--
ALTER TABLE `deck_enrollments_editors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deck_enrollments_users`
--
ALTER TABLE `deck_enrollments_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decks`
--
ALTER TABLE `decks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decks_in_subjects`
--
ALTER TABLE `decks_in_subjects`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
 ADD KEY `user` (`user`(6)), ADD KEY `friend` (`friend`(6));

--
-- Indexes for table `subject_enrollments_editors`
--
ALTER TABLE `subject_enrollments_editors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_enrollments_users`
--
ALTER TABLE `subject_enrollments_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `card_enrollments_editors`
--
ALTER TABLE `card_enrollments_editors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `card_enrollments_users`
--
ALTER TABLE `card_enrollments_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `card_interactions`
--
ALTER TABLE `card_interactions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `cards_in_decks`
--
ALTER TABLE `cards_in_decks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deck_enrollments_editors`
--
ALTER TABLE `deck_enrollments_editors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `deck_enrollments_users`
--
ALTER TABLE `deck_enrollments_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `decks`
--
ALTER TABLE `decks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `decks_in_subjects`
--
ALTER TABLE `decks_in_subjects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `subject_enrollments_editors`
--
ALTER TABLE `subject_enrollments_editors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subject_enrollments_users`
--
ALTER TABLE `subject_enrollments_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;--
-- Database: `intellidu_login`
--
CREATE DATABASE IF NOT EXISTS `intellidu_login` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `intellidu_login`;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `user_id`, `time`) VALUES
(1, 0, 0),
(2, 0, 0),
(3, 0, 0),
(4, 0, 0),
(5, 0, 0),
(6, 0, 0),
(7, 2, 1417213960),
(8, 2, 1417214020),
(9, 2, 1417214235),
(10, 2, 1417214349),
(11, 2, 1417215223),
(12, 6, 1417225178),
(13, 6, 1417226161),
(14, 6, 1417226220),
(15, 8, 1417226705);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(129) NOT NULL,
  `salt` varchar(129) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`) VALUES
(1, 'johnsimer', 'johnsimerlink@gmail.com', '1f04e2f9585d3ca7771a46a19ddbd73038d3818ccb8a0683bbed24c01b310def1e672b16a8f4f09bc330240101cf4d477c6b', '11d7dfdaebaddbfb46d7641b03684bbc2393e316e8b3688ca13744e9674b8286d69a7ae319f648f74d3c3461ac62c650c243'),
(2, 'Account1', 'Account1@gmail.com', 'f26fb559c4523e499d133cc173909766053e778f6be68dbf6d2af6d370447296a2bd4e6b399e3476e540b6d90efaa57ddc73', 'f22a05337edc3c02b410e681e160cd70cc047472afffac63a4c117ef2e9685a69b2eeeed5cf0a00e1ee514f988ec8ab7fd3b'),
(3, 'Account2', 'Account2@gmail.com', 'fac4001d54648107f07eeec92cf0451d3123bc9dccbdb72b5d5d724cafcd4584aeac9e84b62cdf015887812ce3ca7728052e', '4eacf488268499ff23ba19400f5a2c111a390cba425555af89cfa8c082c639d59221a8d4928043af93795d5548a123b44bd8'),
(4, 'Account3', 'Account3@gmail.com', '7ea95614f2c71fc3313244c137fc30c672325bbe995c337b11b7cf85dbd1836df4d7b18301fae15eda0a5057d6dd4fbd7dca', '7fb5493893e65748c95cd7209c15dbbd0e9b3f69242f054b184a4247e2157973fd56f0b8865b52d554c76bf2f9663500800d'),
(5, 'Account4', 'Account4@gmail.com', '3c92a578d7ef1ab549eae2533b80d3045252e650c8de3a8d60277d036d4798bb1611e2b9c4a9c6bf5d348182b042f97e8a84', 'd6c9739462ffafdcfb40b9ee676c0528bb48be17709d9fb79d40e0a3b22723fb754daf5a6375ba80c3ad66ed39c612cefc1c'),
(6, 'Account5', 'Account5@gmail.com', '21b098d15e81533497b4d65d0b6d17aebfa07cb0c0e926f08409e8804ba298c01b88eb1523e329b4596ce03cf6cc61dea801', '07893ef78c96e7c62c378a6928e32e51aa1591604efad77b3ac59d653e8b0a34d2c5691cab860c66cab75de29dae4a425dfb'),
(7, 'Account6', 'Account6@gmail.com', '5079b5b4b734d3ddf9804acafc47e69907edfb412c583a65139804c0f5a512633060ab287a7481ecaf4e69bc94f88b3ed1cd', '7e8f9211f17fcf037130699b56f4eaba63d566f0f4a626da81f656c1059530664870ca5f325ef9a58176cc9bda79f2f55780'),
(8, 'Account7', 'Account7@gmail.com', '6fed908d01c73c49332b0876343fb8dc90ca8c8e5344ab52335de5a2d4e70cdab6211321f47d8bd24594e539f17f034f41db', '0005c95876463b389ff7872bd801ee862ff2d67ba313812a1c30fb2bc153bb2befd8599b6cb0456d3dde52fbf1aee6cae50c'),
(9, 'Account8', 'Account8@gmail.com', 'b0efc5b88ec8b043fb8f1ac16bd9b9fb7e9f8f5d7c02b2c4750cddd431b7899266667b7e7371ef3fe3eda3063a1157794a9503c536e0f25157d38f6dc1b116ce', '229009eccdb85ee3dc102f934b190b58ab63d77116b002d9ade13ffea10c44fc5c339d5e2b10a45bc131f87d615ab4e065f576d71e507fced1b573cd19986964'),
(10, 'Account9', 'Account9@gmail.com', 'a9d3b6e4cbe6814196b3bce298ee74cc6dc75d40efe75f556d93efd0c5c4be27d542d42013eb46c65811ec77c5715e3837c9f07b008a0988a3ccda0c70aab0bf', 'e4043cc7226e6c66da767d23d8bd090afe56526825e3c56a4d15825883eb7cfe4bfc4cdeece2fd71ece1b9e66fd3d92473209ea556767c161c552efec5c09f92'),
(11, 'Account10', 'Account10@gmail.com', '9919e5ce15b6f903fb85503dc1ff1e0d35864439066dc5e69e3a606ede63a1da3f97da63b956703012ef78fec13e2b331eb98cb187b78f27eef3949688da776b', '5e0951336209f47a430796ca658c103856800187467f8d305fa1037466cce1ddd1d19273582c0fb9d32b7aa72d2f444f3f1e1235073d7b3913c0b5487ab9eedf'),
(12, 'Account12', 'Account12@gmail.com', '55def68cabe6e4ca5cacd7b131c24c5498824ccc1e473049fcaea09318f92d4ee1533798cd50d41b08763aafae27a3f597db5fdad2da715b2f939d90e6e8fd1f', '79149c35ddc0aceb2ad73ac1e182d07823ecea1193cf5bc897b2a034cca04d78bf3f6d4c7930b1f6f33176514de4b4000722fbe1333578981c62efb55944e0a9'),
(13, 'Account13', 'Account13@gmail.com', 'd142b8de681cb15a26cde6fbb9e79828df7076471218826c41ae4f0733184f33027b3af28ba94ab368556310dd7d0f91d43224e06091d95f81f95031dca19b03', '33eca7d3314b0ebb7ce1e48414adcbf4fc501499fd3cf4f1e551ca73c80894e5dc4d1b5e8d3bff41233754dc5a42e54e63be3ef79ad34bf3f73cc98b52200060'),
(14, 'Account15', 'Account15@gmail.com', '349b5caca4df0de6f9d2d4982351c19f5e2ce79c8bc61f1997fe839df045e6b772943d77f0fe9e6c1ab4586ce866c1ec9c9a8bd8571d1f66f097e91e0a4b09a4', '824dffe672c469b8759e6cb876ec1ad4af8a9508787f404f17ba5eeaee0fe5d473f0650727008d5fc0942e9cc124ded6cbcb9a4d43a1b9ec40ac51f63d77967f'),
(15, 'Account16', 'Account16@gmail.com', '26e8a900438ffb68ae577cd98a55318c93e3f447cda25c2fe4256932562678816583d36aeebeb2ce2f34b63eeb4d1d9d2aa09a49f6a4eacdf2b3c6eed7bb3c9d', '53a533db74bac37f12c4372b555249969e0d275805e607ff1961c244633b1816a624800376e88cf6a50237ed35cea0f9b29f0731a7396071c5d784c91abc6d07'),
(16, 'Account127', 'Account127@gmail.com', 'b1e9df78d121f7b039626a863294e3db464b08517168d97867902f487e561d0cbbdbf2f08437f076948ec0c6f9971070758d8a46abae80f247e08ea1d2d1c3a0', '4b2b928be86d1ffd6a039e6cd2719624acbc9e8e24a2f498afe7194adbc57ea22965936128692e3925181c62bd7980fdc998360a8fea4069d4754f6ed90395ed'),
(17, 'Account128', 'Account128@gmail.com', '02f9c2c35db4bf6047482b2443f9ea61ed72b4ec94d30dae55c439b30fe93233cfa7ebd7e234984a479c649b12d31bc0da97af221bf1bf0548bc813ac2976647', '678f169e2df29127713216f2f80e33cb20ba327fecc4a43e280cca92c7a7767c13c1fafe75c61f58381443770415b49d05f593d6c5e9057f0134df31bf27152e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
