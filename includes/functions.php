<?php
//Database stuff

//Table of Contents
/*
sec_session_start()
login($email, $password, $logDB)
checkbrute($user_id, $logDB)
login_check($logDB)
logout()
esc_url($url)
changeEnrollment($dbh, $userId, $subjectId, $type, $action)
createSubject($dbh, $subjectName, $subjectDescription, $creatorId)
*/


//connect to the current main db
require_once 'main_connect.php'; // connects to the main database, but does not initialize any session variables yet.
require_once 'login_connect.php';

$appname = "WikiLearn"; // ...and preference
/*  ___________________
   /                   \
  |        BEGIN        |
  |   LOGIN FUNCTIONS   |
   \___________________/
*/
//TODO: make it so that the sec_session_start()function works. Currently it makes it so that the session variable is cleared each time you go to a new page . . .
//currently we are just using the insecure session_start() function
function sec_session_start() { // Separate TODO: Replace with more secure system
    $session_name = 'sec_session_id';   // Set a custom session name
    $secure = SECURE;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"],
        $cookieParams["domain"],
        $secure,
        $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session
    session_regenerate_id();    // regenerated the session, delete the old one.
}

function login($email, $password, $logDB) {
    // Using prepared statements means that SQL injection is not possible. 
    if ($stmt = $logDB->prepare("SELECT id, username, password, salt 
        FROM members
       WHERE email = ?
        LIMIT 1")) {
        $stmt->bindParam(1, $email);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.

        /* Bind columns */
        $stmt->bindColumn(1, $user_id);
        $stmt->bindColumn(2, $username);
        $stmt->bindColumn(3, $db_password);
        $stmt->bindColumn(4, $salt);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
   //     print_r ($result);
  //      echo "the number of users with that email address is " . count($result) / 4;
        $now = time();
   //     echo "time now is $now and userid is $user_id";
        //echo $result;

 
        // hash the password with the unique salt.
       $password = hash('sha512', $password . $salt);
        if (count($result) == 1 *4) {// if there was one result - a.k.a one user with that email address. Notice times four because there are 4 columns
            // If the user exists we check if the account is locked
            // from too many login attempts 
 
            if (checkbrute($user_id, $logDB) == true) {
                // Account is locked 
                // Send an email to user saying their account is locked
            //    echo "the result of checkbrute was true";
                return false;
            } else {
                // Check if the password in the database matches
                // the password the user submitted.
                if ($db_password == $password) {//notice both of these are hashed
                    // Password is correct!
                    session_start();
                    //start the session. 
                   // sec_session_start();
                    // Get the user-agent string of the user.
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user_id'] = $user_id;
                    // XSS protection as we might print this value
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/", 
                                                                "", 
                                                                $username);
                    $_SESSION['username'] = $username;
                    $_SESSION['login_string'] = hash('sha512', $password . $user_browser);
                    // Login successful.
                    return true;
                } else {
                    // Password is not correct
                 //   echo "the db password was $db_password and the password was $password";
                    // We record this attempt in the database
                    $now = time();
                    $stmt = $logDB->prepare("INSERT INTO login_attempts(user_id, time)
                                    VALUES (?, ?)");
                    $stmt->bindParam(1, $user_id);
                    $stmt->bindParam(2, $now);
                    $stmt->execute();
                    return false;
                }
            }
        } else {
            // No user exists.
            echo "no user exists";
            return false;
        }
    }
}

function checkbrute($user_id, $logDB) {
    // Get timestamp of current time 
    $now = time();
 
    // All login attempts are counted from the past 2 hours. 
    $valid_attempts = $now - (2 * 60 * 60);
 
    $stmt = $logDB->prepare("SELECT time 
                             FROM login_attempts
                             WHERE user_id = ? 
                            AND time > ?");
        $stmt->bindParam(1, $user_id);
        $stmt->bindParam(2, $valid_attempts);
 
        // Execute the prepared query. 
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
 
        // If there have been more than 5 failed logins 
        if (count($result) > 5 * 1) { // times 1 because we are only selecting one column
            return true;
        } else {
            return false;
        }
}

function login_check($logDB) {
    // Check if all session variables are set 
    if (isset($_SESSION['user_id'], 
                        $_SESSION['username'], 
                        $_SESSION['login_string'])) {
 
        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];
 
        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
 
        if ($stmt = $logDB->prepare("SELECT password 
                                      FROM members
                                      WHERE id = ? LIMIT 1")) {
            // Bind "$user_id" to parameter. 
            $stmt->bindParam(1, $user_id);
            $stmt->execute();   // Execute the prepared query.
 
            if ($stmt->rowCount() == 1) {//TODO: stmt->rowCount() isn't portable for SELECT statements beyond MySQL. would have to count rows differently
                // If the user exists get variables from result.
                $stmt->bindColumn(1, $password);
                $result = $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);
 
                if ($login_check == $login_string) {
                    // Logged In!!!! 
                    return true;
                } else {
                    // Not logged in 
                    return false;
                }
            } else {
                // Not logged in 
                return false;
            }
        } else {
            // Not logged in 
            return false;
        }
    } else {
        // Not logged in 
        return false;
    }
}

/*  ___________________
   /                   \
  |         END         |
  |   LOGIN FUNCTIONS   |
   \___________________/
*/

function logout(){
	session_start();
	 
	// Unset all session values 
	$_SESSION = array();
	 
	// get session parameters 
	$params = session_get_cookie_params();
	 
	// Delete the actual cookie. 
	setcookie(session_name(),
	        '', time() - 42000, 
	        $params["path"], 
	        $params["domain"], 
	        $params["secure"], 
	        $params["httponly"]);
	 
	// Destroy session 
	session_destroy();
}


function esc_url($url) { // cleans up relative url calls to avoid XSS attacks (adapted from WordPress fn)
 
    if ('' == $url) {
        return $url;
    }
 
    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
 
    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;
 
    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }
 
    $url = str_replace(';//', '://', $url);
 
    $url = htmlentities($url);
 
    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);
 
    if ($url[0] !== '/') {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}


/*  - - - - Subject Enrollment Functions - - - - */
function changeEnrollment($dbh, $userId, $subjectId, $type, $action){
	if($type == 'user' && $action == 'enroll'){
        $sql = "INSERT INTO subject_enrollments_users (subject_id, user_id, progress) VALUES (:lastSubjectId, :userId, '0')";
        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(':lastSubjectId' => $subjectId, ':userId' => $userId));
        echo "user just enrolled performed";
	}
	elseif($type == 'user' && $action == 'unenroll'){
        $sql = "DELETE FROM subject_enrollments_users WHERE user_id = :userId AND subject_id = :subjectId";
        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(':userId' => $userId, ':subjectId' => $subjectId));
	}

	elseif($type == 'editor' && $action == 'enroll'){
        $sql = "INSERT INTO subject_enrollments_editors (subject_id, editor_id ) VALUES (:lastSubjectId, :userId)";
        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(':lastSubjectId' => $subjectId, ':userId' => $userId));
    }
	elseif($type == 'editor' && $action == 'unenroll'){
	}
}

function createSubject($dbh, $subjectName, $subjectDescription, $creatorId){
	$sql = "INSERT INTO subjects (name, description, creator_id) VALUES (:subjectName, :subjectDescription, :creatorId)";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array(':subjectName' => $subjectName, ':subjectDescription' => $subjectDescription, ':creatorId' => $creatorId));
	
	return $dbh->lastInsertId('id');
}
/* - - - - - - - - - - - - - - - - - - - - - - - - */