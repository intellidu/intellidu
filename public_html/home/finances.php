<html>
<head>
</head>
<body>
<h1> Our Finances </h1>
<p> At Intellidu, we want to be financially transparent, for the good of the learning community.
Below are our expenses. Note: these may not be in real time.</p>
<h2> Expenses Timeline </h2>
<iframe width="909" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1z2MJ1ZIWypherDGFeZPBYWLmMYQKtP2vq2HmU-jcOL4/pubchart?oid=492941761&amp;format=interactive"></iframe>
<h2> Expenses Pie Chart </h2>
<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1z2MJ1ZIWypherDGFeZPBYWLmMYQKtP2vq2HmU-jcOL4/pubchart?oid=410837620&amp;format=interactive"></iframe>

<p> Proudly Powered by Google Sheets Publish Chart feature. </p>
</body>
</html>