<?php 
include_once "../../includes/login_connect.php"; // These two files are necessary
include_once "../../includes/functions.php"; //     to determine login status on ANY page.

session_start();

?>

<html>
<head>
	<title>Login Server Testing</title>
</head>
<body>
	<?php
	if (isset($_GET["error"]))
	{
		echo "Error 1: Unable to login.";
	}
	if (login_check($logDB) == true) {
    	$logged = 'in';
	} else {
    	$logged = 'out';
	}
	?>
	<p> <a href="login.php">Login Here!</a> </p>
	<p> Don't have an account? <a href="register_new.php">Register Here!</a> </p>
	<p> You are currently logged <?php echo $logged ?>. </p>
</body>
</html>