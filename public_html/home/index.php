<?php //home/intex.php
require_once '../header.php';
?>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
        <div class="item active">
          <img src="./Carousel Template · Bootstrap_files/slide-01.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>What is Intellidu?</h1>
              <p class="lead">Intellidu is designed so that everyone can learn the quickest way possible in any subject. It lets you memorize up to 2X faster by spacing your flashcards for review at just the right time. This means you aren't studying too much or too little.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="./Carousel Template · Bootstrap_files/slide-02.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>Who Can Make Cards?</h1>
              <p class="lead">Anyone can create courses on our app. Likewise, through a process similar to Wikipedia, anyone can edit courses to make them better, following a set of standards developed from education research.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="./Carousel Template · Bootstrap_files/slide-03.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>What Can Course Creators Do?</h1>
              <p class="lead">A Course creators can optimize their courses through experiments. If Jim creates a Spanish course, he could assign a small portion of his users to a different version of his course, to see if that variation of the course makes students learn quicker.</p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="http://getbootstrap.com/2.3.2/examples/carousel.html#myCarousel" data-slide="prev">‹</a>
      <a class="right carousel-control" href="http://getbootstrap.com/2.3.2/examples/carousel.html#myCarousel" data-slide="next">›</a>
    </div><!-- /.carousel -->



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">
    <h1 align="center">Who We Are</h1><br>
      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="span4">
          <img class="img-circle" data-src="holder.js/140x140" alt="140x140" src="../img/nik.jpg" style="width: 140px; height: 140px;">
          <h2>Nikhil Sinha</h2>
          <p>Nikhil Sinha is a freshman at the University of Cincinnati and is the lead front-end developer of Intellidu.  He enjoys helping others learn and wants to make learning simpler and a quicker process.</p>
          <p><a class="btn" href="https://www.linkedin.com/profile/view?id=325086907&authType=NAME_SEARCH&authToken=fI-2&locale=en_US&trk=tyah2&trkInfo=tarId%3A1416673744859%2Ctas%3Anikhil%20sinha%2Cidx%3A1-3-3">View Linkedin »</a></p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img class="img-circle" data-src="holder.js/140x140" alt="140x140" src="../img/john.jpg" style="width: 140px; height: 140px;">
          <h2>John Simerlink</h2>
          <p>John Simerlink is a freshman at Ohio State University, and is currently leading the Intellidu project. He wants to make learning the highest quality and most efficient it can be. He first began programming actively in ninth grade, and has led the development of several websites and web applications with the INTERalliance of Greater Cincinnati.</p>
          <p><a class="btn" href="https://www.linkedin.com/profile/view?id=194072979&authType=NAME_SEARCH&authToken=NhUc&locale=en_US&trk=tyah2&trkInfo=tarId%3A1416673690978%2Ctas%3Ajohn%20simer%2Cidx%3A1-1-1">View Linkedin »</a></p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img class="img-circle" data-src="holder.js/140x140" alt="140x140" src="../img/rahul.jpg" style="width: 140px; height: 140px;"><!--WHERE is holder.js even coming from/saved in??? -->
          <h2>Rahul Mukherjee</h2>
          <p>Rahul Mukherjee is a freshman at Ohio University, and is developing the user experience for Intellidu. After working on nationally-recognized data visualization projects, he wants to focus his work on education. He has worked in various print and digital design mediums, and he even has experience in programming.</p>
          <p><a class="btn" href="http://lnkd.in/buc-7qF">View Linkedin »</a></p>
        </div><!-- /.span4 -->
      </div><!-- /.row -->

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#navbar">Back to top</a></p>
        <p class="pull-left"><a href="#">About</a> | <a href="#">Contact</a> | <a href="#">Jobs</a></p>
      </footer>

    </div><!-- /.container -->



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- TODO: determine which of these files are already imported via the header.php file -->
    <script src=<?php echo $URL_ROOT?>/js/jquery-2.1.3.min.js></script>
    <script src=<?php echo $URL_ROOT?>/js/bootstrap.min.js></script>
    <!--<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <![endif]-->

    <!-- JQuery and CSS Bootstrap scripts -->
    <!--<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>-->
    <script src=<?php echo $URL_ROOT?>/js/bootstrap.min.js></script> <!-- TODO: <<< Boostrap 3 . .. bootstrap3's css doesn't work with our menu/carousel for some reason :( -->
    <!-- <script src=<?php echo $URL_ROOT?>/css/bootstrap/js/bootstrap-responsive-2.3.2.min.js>
    <script src=<?php echo $URL_ROOT?>/css/bootstrap/js/bootstrap-2.3.2.min.js></script>-->

      <!-- Script for menu-->
      <script src=<?php echo $URL_ROOT?>/home/script.js></script>
      <!-- Login and Registration Processing Scripts -->
      <script src=<?php echo $URL_ROOT?>/js/sha512.js></script> 
      <script src=<?php echo $URL_ROOT?>/js/forms.js></script> 






    <script src="./Carousel Template · Bootstrap_files/bootstrap-transition.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-alert.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-modal.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-dropdown.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-scrollspy.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-tab.js"></script>

    <script src="./Carousel Template · Bootstrap_files/bootstrap-tooltip.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-popover.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-button.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-collapse.js"></script>
    <script src="./Carousel Template · Bootstrap_files/bootstrap-carousel.js"></script>

    <script src="./Carousel Template · Bootstrap_files/bootstrap-typeahead.js"></script>

    <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
    <script src="./Carousel Template · Bootstrap_files/holder.js"></script>

<img id="hzDownscaled" style="position: absolute; top: -10000px;">
