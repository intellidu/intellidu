<?php
session_start();
//Note header.php must always be included by a file that is located in a subdirectory of public_html(e.g. it must be included by a file that is located in the home or app directory)
require_once '../../includes/functions.php';
require_once '../home/register.php'; //included only to get the register_error_msg at the start. also because of the registration form submission.

/* = = = = = ============================= Construct the Header ============================ = = = = = */ 
  /* - - - - Logic to determine what is displayed in the menu based on the user's login status - - - - */
  $register_error_msg="";
  if (login_check($logDB) == true) {
      $logged = 'in';
  } else {
      $logged = 'out';
  }
  //echo "You are logged " . $logged;

  $loggedStatusMessage = "";
  if($logged == 'in') { 
  	$loggedStatusMessage = "Welcome " . $_SESSION['username'] .", y";

  }
  else if ($logged == 'out') { 
  	$loggedStatusMessage = "Y";
  }
  	//echo $logInMenu;
  $loggedStatusMessage.= "ou are logged $logged.";
  if (isset($_GET['error'])) {
      $loggedStatusMessage = '<p class="error">Error Logging In!</p>';
  }

  if($logged == 'in'){
  	$signUpMenu = <<<END
  						<li><a>$loggedStatusMessage</a></li>
  						<li><a href=$URL_ROOT/app>Study</a></li>
  						<li><a href=$URL_ROOT/app/home">Who We Are</a></li>
  	          <li><a href=$URL_ROOT/app/home>Contact</a></li>
  						<li><a id='#logout' href = $URL_ROOT/home/logout.php>Logout</a></li>

END;
  }

  else if($logged == 'out'){
  	$signUpMenu = <<<END
  		            <li><a>$loggedStatusMessage</a></li>
  		            <li><a href=$URL_ROOT/app>Study</a></li>
  					<li><a href=$URL_ROOT/home>Who We Are</a></li>
  	            	<li><a href=$URL_ROOT/home>Contact</a></li>
  	              	<li class="dropdown">
                    <a class="dropdown-toggle signUpLogIn-toggle" data-toggle="dropdown">Sign Up/Log In <b class="caret"></b></a>
                    <ul class="dropdown-menu signUpLogIn-menu">
  	                	<li class="nav-header">Login</li>       
  	                    <form action=$URL_ROOT/home/process_login.php method="post" name="login_form"> 
  		                    <li><input id="logInEmail" type="text" placeholder="example@email.com" name="email"></li>
  		                    <li><input id="logInPassword" type="password" placeholder="password" name="password"></li>
  		                    <li><a id ='logInButton' class="btn"><input type="button" value="Login >>" onclick="formhash(this.form, this.form.password);"/></a></li> 
  		                    <!--TODO: Use JQuery to instead of having a form where i need an input type button, to instead have it send the data went there is a click on the id logInButton>
  		                    <!--TODO: edit the css to make the buttons have a better dynamic width when their is a mobile viewport. -->
  		                </form>
  	                	<li class="divider"></li>
  	                	<li class="nav-header">Sign Up</li>
  						<form action=$_SERVER[PHP_SELF] method="post" name="registration_form">		                    
  		                    <li> $register_error_msg</li>
                          <li><input id="signUpEmail" name='email' type="email" placeholder="example@email.com"></li>
                          <li><input id="signUpUsername" name='username' type="text" placeholder="username"></li> 
  		                    <li><input id="signUpPassword" name= 'password' type="password" placeholder="password"></li>
  		                    <li><input id="signUpConfirm" name = 'confirmpwd' type="password" placeholder="confirm password"></li>
  		                    <li><input id='receiveEmails' type="checkbox" checked> Receive emails from Intellidu</li>
  		                    <li><a id ='signUpButton' class="btn" ><input type="button" value="Sign Up" onclick="return regformhash(this.form,
                                     this.form.username, this.form.email, this.form.password, this.form.confirmpwd);"/> </a></li> 
  							<!--TODO: edit the css to make the buttons have a better dynamic width when their is a mobile viewport. -->
  		                    <!--TODO: Use JQuery to instead of having a form where i need an input type button, to instead have it send the data went there is a click on the id logInButton>
         					</form>
  	                  </ul>
  	                </li>
END;
  }
  /* - - - - ---------------------------------------------------------------------------- - - - - */


  /* - - - - Build the header, inserting in the above constructing variables storing html - - - - */
$header = <<<html
<!DOCTYPE html>
<!-- saved from url=(0052)http://getbootstrap.com/2.3.2/examples/carousel.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Intellidu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
   <!--<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>--> <!--TODO: FIX: <<<cdn not working with or without the http . . .UGH -->
    <!--<link rel="stylesheet" type="text/css" href="$URL_ROOT/css/bootstrap.min.css"/>-->
    <!--<link rel="stylesheet" type="text/css" href="$URL_ROOT/css/bootstrap-responsive.min.css"/>--><!-- TODO: FIX: BOOTSTRAP 3 <<< Doesn't work :( -->
    <link rel="stylesheet" type="text/css" href="$URL_ROOT/css/bootstrap/css/bootstrap-2.3.2.min.css"/>
    <link rel="stylesheet" type="text/css" href="$URL_ROOT/css/bootstrap/css/bootstrap-responsive-2.3.2.min.css"/>
    <!--TODO: use some loading script to load local if cdn isn't working AND to cache the files to reduce future loading times -->
    <!-- Responsive CSS for Navbar and carousel --> <!--TODO: fix arrows thing with mobile carousel (arrows cover up menu when menu is dropped down) -->
     <link href=$URL_ROOT/home/style.css rel="stylesheet" type="text/css" />
    
  <body>

    <!-- NAVBAR
    ================================================== -->
    <div id = 'navbar' class="navbar-wrapper">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container">

        <div class="navbar navbar-inverse">
          <div class="navbar-inner">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="$URL_ROOT/home/"><img class="logo" src="$URL_ROOT/img/logo.gif" /></a>
            <!-- <a class="brand" href="$URL_ROOT/home/index.html#">Intellidu</a> -->
            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
            <div class="nav-collapse collapse">
              <ul class="nav menu">
        $signUpMenu

              <!-- Read about Bootstrap dropdowns at http://twbs.github.com/bootstrap/javascript.html#dropdowns -->                                 
                  
              </ul>
            </div><!--/.nav-collapse -->
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->

      </div> <!-- /.container -->
    </div><!-- /.navbar-wrapper -->
html;
  /* - - - - ---------------------------------------------------------------------------- - - - - */
/* = = = = = ============================= ==================== ============================ = = = = = */ 


/* = = = = = ============================== Display the Header ============================= = = = = = */
echo $header;
/* = = = = = ============================= ==================== ============================ = = = = = */ 
