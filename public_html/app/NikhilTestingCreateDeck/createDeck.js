//Global Variables
var houseOfCards = [];		//stores all the cards
var deckName = "Untitled Deck";	//name of the deck

var frontPlaceHolder = "Card Front";
var backPlaceHolder = "Card Back";

function checkNoCards() {			//makes sure there is atleast 1 card to edit
	if (houseOfCards.length == 0) {
		createNewCells();
	}
}

function updateDeckName(newDeckName) {
	deckName = newDeckName;
}

function createNewCells() {
	houseOfCards.push(new Card("", ""));			//set defaults
	
	//Grab all the HTML from all the cards
	var html = "";
	for (i in houseOfCards) {
		html = html + houseOfCards[i].getHTML();
	}
	document.getElementById("cards").innerHTML = html;			//display the cards to the window
	
	saveAll();
	
	var number = houseOfCards.length-2;
	document.getElementById("button"+number.toString()).focus();		//reset the focus to the next card's front
	
	saveAll();
}

function updateCardFront(number, newFront) {
	houseOfCards[number].setFront(newFront);
	document.getElementById("cardFront"+number.toString()).value = newFront;
}

function updateCardBack(number, newBack) {
	houseOfCards[number].setBack(newBack);
	document.getElementById("cardBack"+number.toString()).value = newBack;
}

function deleteCells(number) {				//delete number from houseOfCards[]
	var houseLength = houseOfCards.length;
	houseOfCards = [];
	for (var i = 0; i < houseLength; i ++) {
		if (i != number) {
			var fr = document.getElementById("cardFront"+i.toString()).value;
			var ba = document.getElementById("cardBack"+i.toString()).value;
			houseOfCards.push(new Card(fr, ba));
		}
	}
	
	checkNoCards();
	var html = "";
	for (var i = 0; i < houseOfCards.length; i ++) {
		html = html + houseOfCards[i].getHTML();
	}
	document.getElementById("cards").innerHTML = html;
}


var inBetweenFrontBack = "{[(EasterEgg)]}";
var inBetweenCards = "{[(!EasterEgg)]}";
function saveAll() {
	var totalString = "";
	for (var i = 0; i < houseOfCards.length; i ++) {
		totalString = totalString + houseOfCards[i].getFront();
		totalString = totalString + inBetweenFrontBack;
		totalString = totalString + houseOfCards[i].getBack();
		if (i != houseOfCards.length-1) {
			totalString = totalString + inBetweenCards;
		}
	}
	document.getElementById("tests").value = totalString;
}

function save() {
	if (deckName == "" || deckName == null) {
		alert("Deck must have a name");
		document.getElementById("deckName").focus();
		return;
	}
	
	//Put AJAX here
}







//Class Card
function Card(front, back) {
	this.front = front;
	this.back = back;
	this.number = houseOfCards.length;
	this.getFront = function() {
		return this.front;
	};
	this.setFront = function(newFront) {
		this.front = newFront;
		document.getElementById("cardFront"+this.number).value = this.front;
	};
	this.getBack = function() {
		return this.back;
	};
	this.setBack = function(newBack) {
		this.back = newBack;
		document.getElementById("cardBack"+this.number).value = this.back;
	};
	this.getHTML = function() {
		return "<li> \
					<textarea name='front+'"+this.number+"' class='card' id='cardFront"+this.number+"' value='"+this.front+"' onKeyUp='updateCardFront("+this.number+", this.value)' placeholder='"+frontPlaceHolder+"'></textarea> \
					<textarea name='back+"+this.number+"' class='card' id='cardBack"+this.number+"' value='"+this.back+"' onKeyUp='updateCardBack("+this.number+", this.value)' placeholder='"+backPlaceHolder+"'></textarea> \
					<!--<input name='front+"+this.number+"' class='card' id='cardFront"+this.number+"' type='text' value='"+this.front+"' onKeyUp='updateCardFront("+this.number+", this.value)' placeholder='"+frontPlaceHolder+"'> \
					<input name='back"+this.number+"' class='card' id='cardBack"+this.number+"' type='text' value='"+this.back+"' onKeyUp='updateCardBack("+this.number+", this.value)' placeholder='"+backPlaceHolder+"'>--> \
					<button class='button' id='button"+this.number+"' onClick='deleteCells(this.value)' value='"+this.number+"'>X</button> \
				</li>";
	};
}



//Key Bindings
document.onkeydown = checkKey;

function checkKey() {
	if (window.event && window.event.keyCode == "9" && window.event.keyCode != "16") {
		var number = houseOfCards.length-1;
		if (document.getElementById("cardBack"+number.toString()) === document.activeElement){
			createNewCells();
		}
	}
}