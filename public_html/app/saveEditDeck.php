<?php
//getDecks.php
/*
			        data: {deck: getAllCardsJSON(), deckName: <?php echo $deckName ?> , deckId: <?php echo $deckId ?>, subjectId: <?php echo $subjectId ?>*/
require_once '../../includes/functions.php';


/*
			        data: {deck: getAllCardsJSON(), deckName: $("#deckName").text(), deckDescription: $("#deckDescription").text(), deckId: <?php echo $deckId ?>, subjectId: <?php echo $subjectId ?> }*/
//TODO: again make sure that they can only edit the deck if they are an editor of the deck.
$deck = json_decode($_GET['deck'], true); //converts json data sent over from the javascript, into a php array.
$deckName = $_GET['deckName'];
$deckDescription = $_GET['deckDescription'];
$deckId = $_GET['deckId'];
$subjectId = $_GET['subjectId'];

//print_r($deck);

$i = 0;

foreach ($deck as $card){

    		$sql = "INSERT INTO cards (id, front, back) VALUES (:cardId, :front, :back) ON DUPLICATE KEY UPDATE front = :front2, back = :back2";
    		$stmt = $dbh->prepare($sql);
    		$stmt->execute(array(':cardId' => $card['cardId'], ':front' => $card['front'], ':back' => $card['back'], ':front2' => $card['front'], ':back2' => $card['back']));

}


//save deck name and description

//if new rows were added insert new cards

//if rows were simply edited, update

//figure out what to do for swapping

$dbh = null; //close the connection string