<?php
session_start();
//GET Requests from AJAX call
	$userId = $_SESSION['user_id'];
	$deckId = $_GET['deckId'];
	$currentTime = $_GET['currentTime'];
	$currentTime = gmdate("Y-m-d H:i:s", $currentTime); //converts to datetime form for mysql

require_once '../../includes/functions.php';

//QUERY: Get any cards that are overdue for review
	//select the card front, card back, card id, where the next_review_time (in card_interactions) is not NULL and is <= current time, for all the cards which belong in the current deck (this automatically assumes that hte user is enrolled in the deck)
	$sql = "SELECT id, front, back, next_review_time FROM cards INNER JOIN (SELECT ceu.card_id, ceu.next_review_time FROM card_enrollments_users ceu WHERE ceu.user_id = :userId AND  ceu.next_review_time <= :currentTime) cards_to_review ON cards.id = cards_to_review.card_id ORDER BY next_review_time DESC"; //order by descending because the getReviewCardFromDB function in hte javascript will order them backwards.
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array(':userId' => $userId, ':currentTime' => $currentTime));

//retun the results in JSON
	$result = $stmt->fetchAll();
	echo json_encode($result);

//close the connection string	
	$dbh = null;