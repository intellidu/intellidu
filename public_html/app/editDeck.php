<?php
$deckId = htmlentities(intval($_GET['deckId']));
//besure to parametrize those^^^ if ever using them in queries
//if there is no get request for a specific deck, redirect the user back to the specific subject page

if (empty($deckId))
 header('Location: viewSubject.php?subjectId=$subjectId'); 

$deckName = $_GET['deckName'];
$subjectId = intval($_GET['subjectId']);

//get deck description
		require_once '../../includes/functions.php';
		$sql = "SELECT d.description d_description
				FROM decks d
				WHERE d.id= :deckId";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':deckId' => $deckId));
		$row = $stmt->fetch();
		$deckDescription = $row['d_description'];
		$dbh = null;

//TODO: if userid is not an editorid than redirect that user back the main page for the subject.
?>

<!DOCTYPE html>
<html>
	<head>
		<!--<script type="text/javascript" language="javascript" src="editDeck.js"></script>-->
        <link rel="stylesheet" type="text/css" href="study_newcard.css">
        <script> 
			//load values from any relevant php variables into javascript variables
			var theDeckId = <?php echo $deckId; ?>;
			var subjectId = <?php echo $subjectId; ?>;
		</script>
		<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src='editDeck.js'></script>
	</head>
	<body id="body">
		<div align="center">
			<input id="deckName" type="text" onKeyUp="updateDeckName(this.value)" onBlur="updateDeckName(this.value)" value=<?php echo $deckName ?> placeholder="Deck Name">
		</div>
		<div align="center">
			<textarea id='deckDescription' class="card" onKeyUp="updateDeckDescription(this.value)" onBlur="updateDeckDescription(this.value)" placeholder="Deck Description"><?php echo $deckDescription ?></textarea>
		</div>
		<br />
		<br />
		<table align="center">
			<tr>
				<td><ul align="center" id="cards"></ul></td>
			</tr>
			<tr>
				<div align="center">
					<td>
						<button class="add" title="add" onClick="createNewCells()">+ Add Card</button>
						<button id = "save" name="save" value="Save">Save</button> <span id='saveInfo'<span id='autosaving'></span>Deck Saved <span id='minutesLastSaved'><!-- in the code round up the minutes after 30 sec --></span> ago</span>
						<input id = 'tempCardId' name = 'tempCardId' type = 'hidden'>
					</td>
				</div>
			</tr>
		</table>
	</body>
</html>
