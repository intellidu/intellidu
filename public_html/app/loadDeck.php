<?php //TODO: check if userId is an Editor for this specific deck

		$deckId = $_GET['deckId'];

		require_once '../../includes/functions.php';

		$sql = "SELECT c.front front, c.back back, c.id id
				FROM cards c, cards_in_decks cid
				WHERE cid.deck_id = :deckId AND cid.card_id = c.id";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':deckId' => $deckId));
		
		$result = $stmt->fetchAll();
		echo json_encode($result);

		$dbh = null;