<?php
/** ==== createSubject.php ====
Creates subject, enrolls creator in the subject and as an editor in the subject, and 
echoes out the created subject (and its description and the creators' progress (0) and number enrolled (1))
*/

//initialization
	session_start();
	$userId = $_SESSION['user_id'];

	require_once '../../includes/functions.php';

	$subjectName = $_GET['subjectName'];
	$subjectDescription = $_GET['subjectDescription'];

//program logic

	//1. create subject and obtain the id of the subject
	$lastSubjectId = createSubject($dbh, $subjectName, $subjectDescription, $userId);

	//2. enroll creator of the course as a user in the course
	changeEnrollment($dbh, $userId, $lastSubjectId, 'user', 'enroll');

	//3. enroll creator of the course as an editor of the course.
	changeEnrollment($dbh, $userId, $lastSubjectId, 'editor', 'enroll');

	//4. -close the connection string
	$dbh = null;

	//5. echo out subject and info so that it can be added to the subject list in real time
	//TODO: Change the echo string to JSON, instead of preformatted HTML.
	echo <<<END
<tr class="subjectRow">
	<td><a class="noFormatting" href="viewSubject.php?subjectId=$lastSubjectId">$subjectName</a></td>
	<td><a class="noFormatting" href="viewSubject.php?subjectId=$lastSubjectId">$subjectDescription</a></td>
	<td><a class="noFormatting" href="viewSubject.php?subjectId=$lastSubjectId">0</a></td><!--TODO:<<<<Change the progress from 0 to whatever the real progress may be, based on the sum of the progresses of each individual card in all the decks. because when a subject is initally created, a user may already have made progress in it if some of the decks and cards are shared from other decks and subjects-->
	<td><a class="noFormatting" href="viewSubject.php?subjectId=$lastSubjectId">1</a></td>
	<td id='unenrollFromSubject'><a class="noFormatting" href='changeEnrollment.php?subjectId=$lastSubjectId&action=unenroll&type=user'><img src=$URL_ROOT/img/remove.jpg title='Unenroll'></a></td>
</tr>
END;


