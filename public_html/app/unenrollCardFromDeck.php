<?php //unenrollCardFromDeck.php
require_once '../../includes/functions.php';

$cardId = $_GET['cardId'];
$deckId = $_GET['deckId'];

    		$sql = "DELETE FROM cards_in_decks WHERE card_id = :cardId AND deck_id = :deckId";
    		$stmt = $dbh->prepare($sql);
    		$stmt->execute(array(':cardId' => $cardId, ':deckId' => $deckId));

    		$dbh = null;
        //TODO: Maybe instead of sending all that data over GET, do it over POST


