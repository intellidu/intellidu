<?php //app/index.php
require_once '../header.php';


?>
<script src=<?php echo $URL_ROOT?>/js/jquery-2.1.3.min.js></script> <!-- ugh. including jquery here again, even though i really shouldn't be. doing so fixes a bug . . . -->
<!--TODO: figure out if there are any problems with placing another script right in the middle of the boddy -->
<script>

$(document).ready(function(){  

  console.log("The document is ready");
  var initialNumberOfSubjectsToShow = 10;

  //load the first subjects that the user is enrolled in
  loadSubjects(0, initialNumberOfSubjectsToShow, 'enrolled');
  loadSubjects(0, initialNumberOfSubjectsToShow, 'not_enrolled');


/* ======== FUNCTIONS ======== */
  //only show the load all subjects button, if there are more than 10 subjects
 // if($("#enrolledSubjectsTable tr").length < initialNumberOfSubjectsToShow + 1 /*+1 because you have to count the table header as a row*/)
  //     $("#loadAllEnrolledSubjects").hide();
  //TODO: Fix that feature^^. Not working.

  $('.enroll').click(function(){
    console.log ('enroll just clicked');
    changeEnrollment(this.parent().id, 'user', 'enroll');
    });
  $('#enrollbutton24').click(function(){
    console.log ("enroll button just clicked");
    //changeEnrollment(this.parent().id, 'user', 'enroll');
    });
  $('.unenroll').click(function(){
    changeEnrollment(this.parent().id, 'user', 'unenroll');
    });


  //TODO: Generic Javascript question. can you put all of these functions outside of the document.ready and will they word? I belive so, lets try.
  //load the rest of subjects that the user is enrolled in, if the user clicks the button with the id loadAllEnrolledSubjects
  //TODO: FIX: the below click function seems to be broken as of 12/24/2014
  $("#loadAllEnrolledSubjects").click(function(){
    loadSubjects($("#enrolledSubjectsTable tr").length, 18446744073709551610/*<<number intentional. do not change */, 'enrolled');
    $(this).hide(); //make button disappear after being clicked
  });

$("#loadAllNotEnrolledSubjects").click(function(){
    loadSubjects($("#notEnrolledSubjectsTable tr").length, 18446744073709551610/*<<number intentional. do not change */, 'not_enrolled');
    $(this).hide(); //make button disappear after being clicked
  });

  $("#createSubject").click(function(){
    console.log("You just clicked the create subject button");
    $.ajax(
      {
        url:"createSubject.php", 
        success:function(result){
            console.log(result);
            $("#enrolledSubjectsTable tr:first").after(result);
        }, 
        error: function(abc) {
          alert(abc.statusText);
        }, 
        data: {subjectName: $("#subjectName").val(), subjectDescription: $("#subjectDescription").val()},
        cache: false
      }
    );
    if($('#openSubject').is(":checked")){
      window.location.href = $(".subjectRow:first a").attr('href');
    }

    //if ($("#openSubject").val()=="openSubject")
   // if ($('#openSubject').prop('checked'))
    //  window.location.herf = "viewSubject.php?subjectId="
   // alert($("#openSubject").val());
    //TODO: See if creating a subject then clicking the button to show all subjects makes the newly created subject show twice.
  }); //end #createSubject.click function

  //make clicking on table row go to the link listed in the cell of the table row
  $('tr').click(function() {
      alert($(this).find('a').attr('href'));
      window.location = $(this).find('a').attr('href');
      }).hover(function() {
    $(this).toggleClass('hover');
  });


/**
Loads the subjects into their appropriate tables, based on the arguments.
@param {number} theStartingSubjectNumber - e.g. if you put 0 for theStartingSubjectNumber and then 10 for theMaxNumberOfSubjectsToReturn, it will retrieve subjects 0 through 9 (or 10?); if you put 10 for theStartingSubjectNumber and 10 for the MaxNumberOfSubjectsToReturn, it will retrieve subjects 10 through 19 (or 20?);
@param {number} theMaxNumberOfSubjectsToReturn - how many subjects to return
@param {boolean} onlyEnrolledInSubjects
*/
function loadSubjects(theStartingSubjectNumber, theMaxNumberOfSubjectsToReturn, theSubjectEnrollmentType){
    $.ajax(
      {
        url:"getSubjects.php", 
        success:function(result){
            if (theSubjectEnrollmentType == 'enrolled'){
              $("#enrolledSubjectsTable").find('tr:last').after(result);
            }
            else if (theSubjectEnrollmentType == 'not_enrolled'){
              $("#notEnrolledSubjectsTable").find('tr:last').after(result);
            }
            $("#loading").hide();
        }, 
        error: function(abc) {
          alert(abc.statusText);
        }, 
        data: {startingSubjectNumber: theStartingSubjectNumber, maxNumberOfSubjectsToReturn: theMaxNumberOfSubjectsToReturn, subjectEnrollmentType: theSubjectEnrollmentType},
        cache: false
      }
    ); //end ajax call
}

function changeEnrollment(subjectId_arg, type_arg, action_arg){
    console.log("change enrollment called");
    $.ajax(
    {
      type: "POST",
      url:"changeEnrollment.php", 
      success:function(result){
        if (type_arg == 'user'){
          if (action_arg == 'enroll'){
            var subject = $("#".subjectId_arg).html();
            $("#enrolledSubjectsTable").find('tr:first').after(subject);
            $("#notEnrolledSubjectsTable").find('#'.subjectId_arg).remove();                                                                                                                                                                                                                                                                             ;
          }
          else if (action_arg == 'unenroll'){
            var subject = $("#".subjectId_arg).html();
            $("#notEnrolledSubjectsTable").find('tr:first').after(subject);
            $("#enrolledSubjectsTable").find('#'.subjectId_arg).remove();  
          }
        }
      }, 
      error: function(abc) {
        alert(abc.statusText);
      }, 
      data: {subjectId: subjectId_arg, type: type_arg, action: action_arg},
      cache: false
    }
  ); //end ajax call
}






}); //end document.ready
</script>

  <!-- give the header some room --><!--TODO: Figuer out some better way to do this -->
  <br><br><br> 
  <div id='Subjects' class="container">
    <h3>Subjects you are enrolled in.</h3>
    <?php
    if ($logged == 'in'){
      echo <<<html
      <div class="table-responsive">
        <table id='enrolledSubjectsTable' class = "table-striped">
        <tbody>
          <tr id='enrolledSubjectsTableHeader'>
            <th>Subject Name</th>
            <th>Description</th>
            <th>My Progress</th>
            <th>Number Enrolled</th>
            <th></th>
          </tr> 
          <!--AJAX query results are put right into here -->
         </tbody>
        </table>
      </div>
      <button id="loadAllEnrolledSubjects">Load All Enrolled Subjects</button>  
html;
    }
    else if ($logged == 'out'){
      echo "You must be logged in to see which subjects you are enrolled in";
    }
    ?>

    <h3>Subjects you are not enrolled in. </h3>
    <?php
    if ($logged == 'in'){
      echo <<<html
      <div class="table-responsive">
        <table id='notEnrolledSubjectsTable' class = "table-striped">
        <tbody>
          <tr id='notEnrolledSubjectsTableHeader'>
            <th>Subject Name</th>
            <th>Description</th>
            <th>Progress</th>
            <th>Number Enrolled</th>
            <th></th>
          </tr> 
          <!--AJAX query results are put right into here -->
         </tbody>
        </table>
      </div>
    <button id="loadAllNotEnrolledSubjects">Load All Not Enrolled Subjects</button>
    <span id='loading'> . . . Loading Content </span>
html;
    }
    else if ($logged == 'out'){
      echo "You must be logged in to see which subjects you are not enrolled in";
    }
    ?>
<?php 
  if ($logged == 'in'){
    echo <<<html
    <div>
      <h3> Create a New Subject </h3>
      <div>
        Subject Name: <input type="text" id ="subjectName" name ="subjectName" placeholder="Enter subject name here"></br>
        Subject Description: <input type="text" id ="subjectDescription" name ="subjectDescription" placeholder="Enter description here"></br>
        <input type="checkbox" id ="openSubject" name="openSubject" value="openSubject" checked="checked">Open up this subject upon creating it.<br>
        <button id="createSubject"> Create Subject </button>
      </div>
    </div>
  </div>
html;
  }
?>



<!--//TODO: Make it so that you can hit enter and it will be the same as clicking on the button. Maybe we will have to change the div to a form or something. <<OR USE some Jquery
//<!-- Editors: text box where you can type in names or usernames of people you know and it will display them and you click and it lists their name and then a comma after it. this would be done using a javascript function. -->
<?php
require_once '../footer.php';
$dbh = null;