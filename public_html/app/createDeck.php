<?php
require_once '../../includes/functions.php';
session_start();

$userId = $_SESSION['user_id'];
$deckName = $_GET['deckName'];
$deckDescription = $_GET['deckDescription'];
$subjectId = $_GET['subjectId'];
//getAllSubjects= intval($_GET['getAllSubjects']);

    		//1 create deck
    		$sql = "INSERT INTO decks (name, description, creator_id) VALUES (:deckName, :deckDescription, :creatorId)";
    		$stmt = $dbh->prepare($sql);
    		$stmt->execute(array(':deckName' => $deckName, ':deckDescription' => $deckDescription, ':creatorId' => $userId));


    		//2 add the deck as a deck in the subject

    			//get id of the deck that was just created
    			$lastDeckId = $dbh->lastInsertId('id');
    		
    			//add the deck as a deck in that subject
	    		$sql = "INSERT INTO decks_in_subjects (subject_id, deck_id) VALUES (:subjectId, :deckId)";
	    		$stmt = $dbh->prepare($sql);
	    		$stmt->execute(array(':subjectId' => $subjectId, ':deckId' => $lastDeckId));

			//3 enroll the creator of the deck into the deck, so that the creator can study or view the deck
	    	$sql = "INSERT INTO deck_enrollments_users (deck_id, user_id, progress) VALUES (:deckId, :userId, '0')";
	        $stmt = $dbh->prepare($sql);
	        $stmt->execute(array(':deckId' => $lastDeckId, ':userId' => $userId));

	        //TODO: ^^ i'm assigning a progress of 0 for the deck. if some of the cards are shared in multiple decks. 
	        //	then I need to recalculate the user's progress on the deck based on his progress on each of those individual cards or card groups

	    	//4 give permission for the creator of the deck to be an editor of the deck.
	    	$sql = "INSERT INTO deck_enrollments_editors (deck_id, editor_id) VALUES (:deckId, :editorId)";
	        $stmt = $dbh->prepare($sql);
	        $stmt->execute(array(':deckId' => $lastDeckId, ':editorId' => $userId));

    		//5 echo out the deck info back into the list of decks in that subject using ajax
            $dbh = null;
   echo "
        <tr>
    		<td><a class='noFormatting' href='studyDeck.php?deckId=$lastDeckId&subjectId=$subjectId&deckName=$deckName&deckDescription=$deckDescription'>$deckName</a></td>
    		<td><a class='noFormatting' href='studyDeck.php?deckId=$lastDeckId&subjectId=$subjectId&deckName=$deckName&deckDescription=$deckDescription'>$deckDescription</a></td>
    		<td><a class='noFormatting' href='studyDeck.php?deckId=$lastDeckId&subjectId=$subjectId&deckName=$deckName&deckDescription=$deckDescription'>0</a></td><!--TODO:<<<<Change the progress from 0 to whatever the real progress may be, based on the sum of the progresses of each individual card in all the decks. because when a subject is initally created, a user may already have made progress in it if some of the decks and cards are shared from other decks and subjects-->
    		<td><a class='noFormatting' href='studyDeck.php?deckId=$lastDeckId&subjectId=$subjectId&deckName=$deckName&deckDescription=$deckDescription'><img src=".  $URL_ROOT ."/img/edit.jpg title='Edit your subject'></a></td>
    	</tr>
        ";
        //TODO: Maybe instead of sending all that data over GET, do it over POST


