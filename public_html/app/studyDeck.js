//Global Variables
var houseOfCards = [];
var cardsAlreadyScheduledForReview = [];
var place = -1;

//Ids
var cardFrontID = "question";
var cardBackID = "answer";
var confButton = "confButton";
var confButtonTr = "confidenceButtons";
var onCard = "onCard";
var totalCards = "totalCards";

var defaultAnswer = "Tap card to reveal answer";

var startFront, endFront, startBack, endBack;

function displayCard(card) {		//displays a card
	$('#'+cardFrontID).html(card.getFront());
	if (card.backVisible) {				//Answer is visible
		$('#'+cardBackID).html(card.getBack());
	}
	else {
		$('#'+cardBackID).html(defaultAnswer);
	}
}

function toggleAnswerVisible() {			//toggles the visibility of the answer
	document.getElementById(confButtonTr).style.pointerEvents = "auto";	//make confidence buttons abled
	
	if (houseOfCards[place].backVisible) {
		houseOfCards[place].setBackVisible(false);
	}
	else{
		houseOfCards[place].setBackVisible(true);
	}
	displayCard(houseOfCards[place]);
}

function selectNextCard(){
	//query db to see if a review card is scheduled	
	//if there is or are review cards scheduled, it adds those card(s) as the next cards, at position place + 1.
	loadReviewCards();

	//create some logic based on the results of if htere were any wevier cards or not.

	goToNextCard();
}

function goToNextCard() {					//flip to the next card; make answer blank; make current card backVisible=false
	document.getElementById(confButtonTr).style.pointerEvents = "none";		//make confidence buttons disabled
	
	if (place == -1 && houseOfCards != null) {
		place = 0;
	}
	else if (place < houseOfCards.length) {		//not past the length of houseOfCards
		place ++;
		houseOfCards[place-1].visible = false;
		houseOfCards[place-1].setBackVisible(false);

	}
	console.log("right before we are going to the next card the value of place is " + place);
	console.log("the card at that place - is it null?: " + $.isEmptyObject(houseOfCards[place]));
	houseOfCards[place].visible = true;
	displayCard(houseOfCards[place]);
	
	$('#'+onCard).html(place+1);	//update "onCard"
	$('#'+totalCards).html(houseOfCards.length.toString());		//update "totalCards"

	startFront = new Date().getTime();
}

function loadReviewCards(){
	$.ajax(
		{url:"nextCard.php", 
	    success:function(result){
	    	console.log("This is the result of the loadReviewCards function: " + result);
	    	loadedCards = result;
			objLoadedCards = jQuery.parseJSON(loadedCards);
        	numCards = Object.keys(objLoadedCards).length;
        	for (var i =0; i < numCards; i++){
			//	alert(i + ": " + objLoadedDeck[i].front);
				//inserts those cards into the next item in the array.
				if (!cardAlreadyExists(objLoadedCards[i].id)){
	        		houseOfCards.splice(place + 1, 0, new Card(objLoadedCards[i].front, objLoadedCards[i].back, objLoadedCards[i].id));
	        		console.log("a card scheduled for review, with id " + objLoadedCards[i].id + " being spliced into next position");
        		}
        	}
	    }, 
	    error: function(abc) {
			 alert(abc.statusText);
	    }, 
	    data: {deckId: deckId, currentTime: new Date().getTime() / 1000}//userId is passed via Session handling
	  }
	);
}

function getCardsScheduledForReview(){
	$.ajax(
		{url:"getCardsScheduledForReview.php", 
	    success:function(result){
	    	console.log("This is the result of the getCardsScheduledForReview function: " + result);
	    	loadedCards = result;
			objLoadedCards = jQuery.parseJSON(loadedCards);
        	numCards = Object.keys(objLoadedCards).length;
        	for (var i =0; i < numCards; i++){
			//	alert(i + ": " + objLoadedDeck[i].front);
				//inserts those cards into the next item in the array.
	        		cardsAlreadyScheduledForReview.push( new Card(objLoadedCards[i].front, objLoadedCards[i].back, objLoadedCards[i].id));
	        		console.log("new card  with id " + objLoadedCards[i].id + " being pushed into cards already scheduled for review");
        	}
	    }, 
	    error: function(abc) {
			 alert(abc.statusText);
	    }, 
	    data: {deckId: deckId, currentTime: new Date().getTime() / 1000}//userId is passed via Session handling
	  }
	);
}


//Card Class
function Card(front, back, cardId) {
	this.front = front;				//text displayed on front of card
	this.back = back; //text displayed on back of card
	this.cardId = cardId;	
	this.frontTime = 0; //how much time the user has spent looking at the front of the card before flipping over	
	this.backTime = 0;	//how much time the user has spent looking at at the card, while the back is flipped over.
	this.timestamp = 0;
	this.numberOfFlips = 0; //number of times the user has flipped the card. if the user hits "Click on question to reveal answer" then that counts as one flip. If the user clicks on the back twice (first to see the back, and then to unsee the back) that counts as two flips.

	this.confidence = 0;

	this.backVisible = false;		//true answer is visible
	this.visible = false;
	this.getFront = function() {
		return this.front;
	};
	this.getBack = function() {
		return this.back;
	};
	this.getCardId = function() {
		return this.cardId;
	};
	this.getFrontTime = function(){
		return this.frontTime;
	}
	this.setFrontTime = function(frontTime){
		this.frontTime = frontTime;
	}
	this.getBackTime = function(){
		return this.backTime;
	}
	this.setBackTime = function(backTime){
		this.backTime = backTime;
	}
	this.getTimestamp = function(){
		return this.timestamp;
	}
	this.setTimestamp = function(timestamp){
		this.timestamp = timestamp;
	}
	this.getNumberOfFlips= function(){
		return this.numberOfFlips;
	}
	this.setNumberOfFlips = function(numberOfFlips){
		this.numberOfFlips = numberOfFlips;
	}
	this.getConfidence = function(){
		return this.confidence;
	}
	this.setConfidence = function(confidence){
		this.confidence = confidence;
	}
	this.setBackVisible = function(newBackVisible) {		//resets this.backVisible
		this.backVisible = newBackVisible;
	};
}

function saveCardInteraction(){
	$.ajax(
		{url:"cardInteraction.php", 
        success:function(result){
        	console.log(result);
        	console.log('Save card successful for deck 1 (sample) for card #'+houseOfCards[place].getCardId());
        }, 
        error: function(abc) {
			 alert(abc.statusText);
        }, 
        data: {cardId: houseOfCards[place].getCardId(), confidence: houseOfCards[place].getConfidence(), timeFront: houseOfCards[place].getFrontTime(), timeBack: houseOfCards[place].getBackTime(), numberOfFlips: houseOfCards[place].getNumberOfFlips(), timestamp: houseOfCards[place].getTimestamp()}//userId is passed via Session handling
      }
	);
}

		var startFront, endFront, startBack, endBack;
			/*	loads cards that have not been scheduled for review yet and that still need to be studied for the first time. */
			function loadDeck(){

				$.ajax(
					{url:"loadDeck.php", 
					async: false,
			        success:function(result){
			        	loadedDeck = result;
			        	console.log("this are the cards from the normal deck that is being loaded. this is before they have been added to the studying session and checked if they have already appeared in the review part" + loadedDeck);
			        	objLoadedDeck = jQuery.parseJSON(loadedDeck);
			        	numCards = Object.keys(objLoadedDeck).length;
			        	for (var i =0; i < numCards; i++){
						//	alert(i + ": " + objLoadedDeck[i].front);
							if (!cardAlreadyExists(objLoadedDeck[i].id)/*&& !cardAlreadyScheduledForReview(objLoadedDeck[i].id)*/) {
								console.log("a card with the id " + objLoadedDeck[i].id + " is about to be pushed to the end of the deck. Ooh does it do it out of order?");
			        			houseOfCards.push(new Card(objLoadedDeck[i].front, objLoadedDeck[i].back, objLoadedDeck[i].id));
			        			console.log(" a card with the id " + objLoadedDeck[i].id + " was just pushed to the study session");
			        		}
			        		else console.log("no cards from the deck were pushed into hte study session, meaning that hte deck was either empty or that the cards being loaded were already loaded as review cards");
			        	}
			        }, 
			        error: function(abc) {
						 alert(abc.statusText);
			        }, 
			        data: {deckId: deckId}//userId is passed via Session handling
			      }
				);
				goToNextCard();
			}

				function storeConfidence(confidence){
					//convert the id from "confidence1" to "1" to integer 1, or from "confidence4" to "4" to integer 4.
					confidence = parseInt(confidence.substr(confidence.length - 1, confidence.length - 1));
					houseOfCards[place].setConfidence(confidence);

				}

				function displayCardStatus(){
					console.log('numberOfFlips: ' + houseOfCards[place].getNumberOfFlips() +', time spent without seeing back of card: ' + houseOfCards[place].getFrontTime() + ', time spent with seeing back of card: ' + houseOfCards[place].getBackTime() + ', confidence: ' + houseOfCards[place].getConfidence());
				}

				function cardAlreadyExists(cardId){
					exists = false;
					for (var i = 0; i <houseOfCards.length; i++){
						if (houseOfCards[i].getCardId() == cardId){
							console.log("the card with id " + cardId + " already exists yo");
							return true;
						}
					}
					return exists;
				}
				function cardAlreadyScheduledForReview(cardId){

				}


				function startTimer(side){
					if (side == 'front')
						startFront = new Date().getTime();
					if (side == 'back')
						startBack = new Date().getTime();
				}

				function endTimer(side){
					if (side == 'front')
						endFront = new Date().getTime();
					if (side == 'back')
						endBack = new Date().getTime();
				}

				function storeTime(type){
					if (type == 'front')
						houseOfCards[place].setFrontTime(houseOfCards[place].getFrontTime() + (endFront - startFront));
					else if(type == 'back')
						houseOfCards[place].setBackTime(houseOfCards[place].getBackTime() + 
							(endBack - startBack));
				}

			$(document).ready(function(){
				loadReviewCards(); //loads cards that have passed their due time for review
				getCardsScheduledForReview(); //gets cards that have been studied before, but whose review times have not occured yet. 
				loadDeck(); //loads the rest of the cards in the deck. these are the cards that have nnot been studied yet. If a card already exists in the cards scheduled for review, then that card is not loaded into houseOfCards via this function.

				$('.Card').click(function(){
					toggleAnswerVisible();	
					//increment number of flips
					houseOfCards[place].setNumberOfFlips(houseOfCards[place].getNumberOfFlips()+1);

					if (houseOfCards[place].getNumberOfFlips() % 2 == 1){// This means the user just flipped the card over so that they can see the back.
						//calculate and set the front time
						endTimer('front');
						storeTime('front');

						//start the timer for the back of the card
						startTimer('back');

					}
					else if(houseOfCards[place].getNumberOfFlips() % 2 == 0){// This means the user just flipped the card over so that they can see the front.

						endTimer('back');
						storeTime('back');
						startTimer('front');
					}
				});
				$('.Circlecircledotdot').click(function(){
					//TODO should only be able to see and click on the conf buttons when backflips mod 2 = 1
					var confidence = this.id;
					storeConfidence(confidence);

					endTimer('back');

						
					storeTime('back');
					houseOfCards[place].setTimestamp(endBack); //sets the timestamp of the card interaction equal to the time that the user clicked on the confidence buttons.


					displayCardStatus();


					saveCardInteraction();
					
					//currently, selectNextCard must be at end so because gotonextcard increments place, which is used in all the other functions.
					selectNextCard();						
				}); 

			});