<?php

session_start();

$subjectId = intval($_GET['subjectId']);
$userId = $_SESSION['user_id'];

//if there is no get request for a specific subject, redirect the user back to the all subjects page
if (empty($subjectId)){
  header('Location: index.php'); 
}

require_once '../header.php';


?>
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>// alert("This is a pretest");
$(document).ready(function(){

  var initialNumberOfDecksToShow = 10;
  //load the first decks in the subject
  //alert("this is a test");
  $.ajax(
      {
        url:"getDecks.php", 
        success:function(result){
            $("#decksTable tbody").append(result);
            $("#loading").hide();
           // alert(result);
        }, 
        error: function(abc) {
          alert(abc.statusText);
        }, 
        data: {bottomLimit: 0, maxNumberOfDecksToReturn: initialNumberOfDecksToShow, subjectId: <?php echo $subjectId ?> }
      }
    );
  
  //only show the load all decks button, if there are more than 10 decks
 if($("#decksTable tr").length < initialNumberOfDecksToShow +1 /*+1 because you have to count the table header as a row*/)
  	$("#loadAllDecks").hide();
  //TODO: Right now this will still show the button even if there are exactly initialNumberOfDecksToShow decks in the database. Make it so that it will not show the button if there are initialNumberOfDecksToShow decks in the database for this subject.
// // $("#loadAllDecks").show();
  //load the rest of subjects that the user is enrolled in, if the user clicks the button with the id loadAllDeck
  $("#loadAllDecks").click(function(){
    $.ajax(
      {
        url:"getDecks.php", 
        success:function(result){
            $("#decksTable tbody").find('tr:last').parent().append(result);
        }, 
        error: function(abc) {
          alert(abc.statusText);
        }, 
        data: {bottomLimit: $("#decksTable tr").length, maxNumberOfDecksToReturn: 18446744073709551610, subjectId: <?php echo $subjectId ?>}, //<<That value is specific. Don't change it. No joke. Has to do with MySQL stuff.
        cache: false
      }
    );
    $(this).hide(); //make button disappear after being clicked
  });

  $("#createDeck").click(function(){
    $.ajax(
      {
        url:"createDeck.php", 
        success:function(result){
            $("#decksTable tbody").prepend(result);
            console.log("result has been pasted");
        }, 
        error: function(abc) {
          alert(abc.statusText);
        }, 
        data: {deckName: $("#deckName").val(), deckDescription: $("#deckDescription").val(), subjectId: <?php echo $subjectId ?>},
        cache: false
      }
    );
    //if ($("#openSubject").val()=="openSubject")
   // if ($('#openSubject').prop('checked'))
    //  window.location.herf = "viewSubject.php?subjectId="
   // alert($("#openSubject").val());
    //TODO: See if creating a subject then clicking the button to show all subjects makes the newly created subject show twice.
  });

  //make clicking on table row go to the link listed in the cell of the table row
  $('tr').click( function() {
    alert($(this).find('a').attr('href'));
      window.location = $(this).find('a').attr('href');
      }).hover( function() {
    $(this).toggleClass('hover');
  });

});
</script>
</head>
<body>
<?php
	//list basic info: name, description.
	//TODO: Add other information like number enrolled, progress etc.
	//TODO: maybe instead of having to requery the database to get the name of the subjects and other related info, we could just send it via JSON from one file to the other.
	//TODO: Add ordering feature for the decks so that 1) you can see what number the deck is 2) so that you can see the decks in the order that they are supposed to be studied 3)So that a course editor with the correct priveleges can rearrange the order of the decks/lessons
		$sql = "SELECT s.name s_name, s.description s_description
				FROM subjects s, subject_enrollments_users se
				WHERE s.id = :subjectId AND se.user_id = :userId
				";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':userId' => $userId, ':subjectId' => $subjectId/*, ':bottomLimit' => $bottomLimit*/));

		$row = $stmt->fetch();
		echo <<<END
	    						subject: $row[s_name]<br>
	    						description: $row[s_description]<br>
END;
	//list all the decks in that subject, and the user's progress on them   
		?>
		<h2> Decks/Lessons in This Subject </h2>
		<table id='decksTable' border='1'>
		<tr>
      <thead>
  			<th> Deck Name </th>
  			<th> Deck Description </th>
  			<th> Your Progress </th>
  			<th></th>
      </thead>
      <tbody>
      </tbody>
		</tr>

		</table>

    <span id='loading'> . . . Loading Content </span>

</body>
</html>

<button id="loadAllDecks">Load All Decks</button>

<!-- TODO: Error happens after you create deck. fix. -->

<div>
<h3> Create a New Deck </h3>
<div>
Deck Name: <input type="text" id ="deckName" name ="deckName" placeholder="Enter deck name here"></br>
Deck Description: <input type="text" id ="deckDescription" name ="deckDescription" placeholder="Enter description here"></br>
<input type="checkbox" id ="openDeck" name="openDeck" value="openDeck" checked="checked">Open up the deck editor upon creating this deck.<br>
<button id="createDeck"> Create Deck </button>
</div>
</div>


<?php

		$dbh = null;
	

//list if you are enrolled and if not click "+Enroll"
//list your progress
//list your overall time spent, list your time spent per progress %, and the average time spent per progress %
//Study Button

//list the deck order
    //if you are an editor than you can edit this order. In the format (23, 46, 24, 25, 28, 29, 30, 31) where those are the deck ids. later it will just be dragging and dropping the order, and it will autosave
//List the decks/lessons in a table format and your progress for each of them

//button to create deck and option to open simple deck editor (the one nikhil has already made)



//TODO: Option to delete decks
//TODO: Two buttons on deck: Study and Edit (
    	//if a user is not an editor of the course then clicking Edit will create a user-specific clone of that deck, rather than editing the master)
    	