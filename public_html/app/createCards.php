<?php
require_once '../../includes/functions.php';
session_start();

//get the information of the card(s)

	//below works for when one card is sent over
		$numberOfCards = $_GET['numberOfCards'];
		$deckId = $_GET['deckId'];
		$front = $_GET['front'];
		$back = $_GET['back'];

		$userId = $_SESSION['user_id'];

//if no id was sent over (e.g. id was 0), that means this is a new card being created
	//YES IN THIS CASE NO ID IS SENT OVER . . .MAY have to vchange the logic or file system for when doing miutpliple cards at once.
	//..currently the way the app is set up it should only be sending one card at a time without an id.
	//.. and currently the front and back should be blank.
	//insert card into cards table
	    $sql = "INSERT INTO cards (front, back) VALUES (:front, :back)";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':front' => $front, ':back' => $back));
	//retrieve id
    	$lastCardId = $dbh->lastInsertId('id');
	//insert card into cards_in_deck table <<<SHOULD I DO THIS HERE?
		$sql = "INSERT INTO cards_in_decks (card_id, deck_id) VALUES (:cardId, :deckId)";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':cardId' => $lastCardId, ':deckId' => $deckId));

	//3 Enroll Creator of Deck as user for the card
		$sql = "INSERT INTO card_enrollments_users (card_id, user_id) VALUES (:cardId, :userId)";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':cardId' => $lastCardId, ':userId' => $userId));

	//4 Enroll creator of deck as editor of card
		$sql = "INSERT INTO card_enrollments_editors (card_id, editor_id) VALUES (:cardId, :user_id)";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':cardId' => $lastCardId, ':user_id' => $userId));
	//echo/return id of the card
		echo $lastCardId;

//if id was sent over, that means this is a currently existing card(s) that is being edited
	//.. these cards being sent over should only be edited cards. a card should not be sent to the server if it was not edited.
	//..TODO: Perhaps later just to be safe, we should have a server side check if the card has indeed been edited

	//for each card, update the information on that card


//TODO: Some message if card wasn't successfully able to be created or edited.

//no deletions or other things in this file yet.