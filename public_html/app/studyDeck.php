<?php
require_once '../../includes/functions.php';
$deckId = $_GET['deckId'];
$subjectId = $_GET['subjectId'];
$deckName = $_GET['deckName'];
//$deckDescription = $_GET['deckDescription'];
//$subjectName = $_GET['subjectName'];
?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="study_style.css">
		<meta charset="UTF-8">
		<title><?php echo $deckName /*. " (" . $subjectName . ")" */?></title>
		<script> var deckId = <?php echo $deckId ?>; </script>
		<script type="text/javascript" language="javascript" src=<?php echo $URL_ROOT;?>/js/jquery-2.1.1.js></script>		
		<script type="text/javascript" language="javascript" src="studyDeck.js"></script>
	</head>
	<body>
		<h1 align="center" id="headline"><?php echo $deckName /*. " (" . $subjectName . ")" */?><h1>
		<table align="center" id="top">
			<tr>
				<td align="right">
					<p class="onCard">Card:</p>
					<p id="onCard" class="onCard">1</p>
					<p style="display: inline; font-size: 25px; color: black; margin-right: -30px; margin-left: -10px">/</p>
					<p id="totalCards" class="onCard">1</p>
				</td>
			</tr>
			<tr>
				<td>
					<div class = 'Card'>
						<div>
							<p id="question">Front of Card</p>
						</div>
						<div>
						------
						</div>
						<div>
							<p id="answer">Tap card to reveal answer</p>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="left" style="font-size: 12px; color: black; width:auto" >
					How Well Did you know this? <br>5 = I knew it perfectly. 1 = I didn't know it at all 
				</td>
			</tr>
		</table>
		
		
		<table align="center" id="bottom">
			<tr id="confidenceButtons">
				<td><button class="Circlecircledotdot" id = 'confidence1'><h3>1</h3></button></td>
				<td><button class="Circlecircledotdot" id = 'confidence2'><h3>2</h3></button></td>
				<td><button class="Circlecircledotdot" id = 'confidence3'><h3>3</h3></button></td>
				<td><button class="Circlecircledotdot" id = 'confidence4'><h3>4</h3></button></td>
				<td><button class="Circlecircledotdot" id = 'confidence5'><h3>5</h3></button></td>
			</tr>
		</table>
	</body>
</html>