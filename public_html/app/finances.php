<html>
<head>
</head>
<body>
<h1> Our Finances </h1>
<p> At Intellidu, we want to be financially transparent, for the good of the learning community.
Below are our expenses. Note: These will be updated quarterly.</p>
<!-- TODO: 1) write a script that automatically updates the ranges of the charts, so that they update when new information is place in the expenses table. 2) may have to update the Google Sheets Chart link, if the chart is a snapshot and not a live-updated sort of deal. These two actions will eliminate any need to manually update the charts ahd publishing link, meaning all our expenses could be in real time, as soon as they are listed in our expenses sheet. -->
<h2> Expenses Timeline </h2>
<iframe width="909" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1z2MJ1ZIWypherDGFeZPBYWLmMYQKtP2vq2HmU-jcOL4/pubchart?oid=492941761&amp;format=interactive"></iframe>
<h2> Expenses Pie Chart </h2>
<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1z2MJ1ZIWypherDGFeZPBYWLmMYQKtP2vq2HmU-jcOL4/pubchart?oid=410837620&amp;format=interactive"></iframe>

<p> Proudly Powered by Google Sheets Publish Chart feature. </p>
</body>
</html>