<?php
//cardInteraction.php
session_start();
$userId = $_SESSION['user_id'];

$cardId = $_GET['cardId'];
$confidence = $_GET['confidence'];
$timeFront = $_GET['timeFront'];
$timeBack = $_GET['timeBack'];
$numberOfFlips = $_GET['numberOfFlips'];
$timestamp = $_GET['timestamp'];

require_once '../../includes/functions.php';

	//0 get card enrollment id for use in a couple queries

	$sql = "SELECT id from card_enrollments_users WHERE user_id = :userId AND card_id = :cardId";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array(':userId' => $userId, ':cardId' => $cardId));
	$row = $stmt->fetch();

	$cardEnrollmentId = $row['id'];


	//1. retrieve all the previous interactions with that card
	$sql = "SELECT interaction_time, confidence FROM card_interactions WHERE card_enrollment_id = :cardEnrollmentId";
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array(':cardEnrollmentId' => $cardEnrollmentId));

	//2. determine the next time that card should be reviewed
		$fiveStreak = 0;
		while ($row = $stmt->fetch()){
			if ($row['confidence'] == 5)
				$fiveStreak++;
			else $fiveStreak = 0;
		}

		$nextReviewTime = calculateReviewTime($confidence, $fiveStreak, $timestamp);

		//returns the next time that the card should be reviewed in yyyy-mm-dd hh:mm:ss format in GMT time
		//Notice that all the other timestamps in the database are currently EST time
		function calculateReviewTime($confidence, $fiveStreak, $timestamp){
			$factorial = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600];
			$minutes = 2*pow(7,($confidence-1)) * $factorial[$fiveStreak];
			$seconds = $minutes * 60;
			$tempNextReviewTime = $timestamp/1000 + $seconds;
			$nextReviewTime = gmdate("Y-m-d H:i:s", $tempNextReviewTime);
			return $nextReviewTime;
		}

	//3. store that time into the database
		$sql = "UPDATE card_enrollments_users SET next_review_time = :nextReviewTime WHERE id = :cardEnrollmentId";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':nextReviewTime' => $nextReviewTime, ':cardEnrollmentId' => $cardEnrollmentId));

	//4. store the study info into the database

	//4a. store individual interaction into card interactions table
		$sql = "INSERT INTO card_interactions (card_enrollment_id, confidence, time_front, time_back, number_of_flips) VALUES ((SELECT id from card_enrollments_users WHERE user_id = :userId AND card_id = :cardId), :confidence, :timeFront, :timeBack, :numberOfFlips)";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':userId' => $userId, ':cardId' => $cardId, ':confidence' => $confidence, ':timeFront' => $timeFront, ':timeBack' => $timeBack, ':numberOfFlips' => $numberOfFlips));
		
	//4b. store confidence into card enrollments table, for quicker access in future (Yes I understand this makes the database unnormalized and redundant. We'll research the pros and cons of having a normalized vs nonnormalized db later or a nonnormalized one that has scripts run automatically to normalize itself).
		$sql = "UPDATE card_enrollments_users SET confidence = :confidence WHERE id = :cardEnrollmentId";
		//TODO get rid of subqueries. apparently they are resource intensive
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':confidence' => $confidence, ':cardEnrollmentId' => $cardEnrollmentId));

		$dbh = null;

	//TODO: create a file that determines what hte next card should be, whether its the next card in the deck you are studying, or a card that is being studied for review.
