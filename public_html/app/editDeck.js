var houseOfCards = [];		//stores all the cards

var frontPlaceHolder = "Card Front";
var backPlaceHolder = "Card Back";

function checkNoCards() {			//makes sure there is atleast 1 card to edit
	//alert("check no cards is being called");
	if (houseOfCards.length == 0) {
		createNewCells();
	}
}

function createNewCells() {
	houseOfCards.push(new Card("", "", 0));			//set defaults
	
	//Grab all the HTML from all the cards
	var html = "";
	for (i in houseOfCards) {
		html = html + houseOfCards[i].getHTML();
	}
	document.getElementById("cards").innerHTML = html;			//display the cards to the window
	
	var number = houseOfCards.length-2;
	document.getElementById("button"+number.toString()).focus();		//reset the focus to the next card's front
			//seem's to be producing an error in the console, if a card is deleted
}

function updateCardFront(number, newFront) {
	houseOfCards[number].setFront(newFront);
	document.getElementById("cardFront"+number.toString()).value = newFront;
}

function updateCardBack(number, newBack) {
	houseOfCards[number].setBack(newBack);
	document.getElementById("cardBack"+number.toString()).value = newBack;
}

//TODO: Make the below more efficient, instead of repopulating hte array everytime . . . 
function deleteCells(number) {				//delete number from houseOfCards[]
	var houseLength = houseOfCards.length;
	houseOfCards = [];
	for (var i = 0; i < houseLength; i ++) {
		if (i != number) {
			var fr = document.getElementById("cardFront"+i.toString()).value;
			var ba = document.getElementById("cardBack"+i.toString()).value;
			var id = document.getElementById("cardId"+i.toString()).value; //note that the card id is not necessarily, and most likely not, i (the number of the card in the temporary houseOfCards Object)
			houseOfCards.push(new Card(fr, ba, id));
		}
		else {
			var id = document.getElementById("cardId"+i.toString()).value;
			unenrollCardFromDeck(id);
		}

	}
	
	checkNoCards();
	var html = "";
	for (var i = 0; i < houseOfCards.length; i ++) {
		html = html + houseOfCards[i].getHTML();
	}
	document.getElementById("cards").innerHTML = html;
}//TODO:When you delete a card, it should shift the cards below up. This will make it more easy to  see which cards were deleted.


//Class Card
function Card(front, back, cardId) { //cardId corresponds with cardId in database
	this.front = front;
	this.back = back;
	this.number = houseOfCards.length;
	//alert("Yo a new card is being created. its hoc number is " + this.number);

	if(cardId === 0 || cardId === undefined){ // if Id = 0, that means that the card is not being loaded from the database, and rather was just created by the user.
		createCardInDb(front, back, this.number);
		this.cardId = $('#tempCardId').val();
		} //creates the card in the database in this deck and all that jazz, and then inside the ajax function calls the card's setCardId function.
	else this.cardId = cardId; //if the cardId was not zero, that means that the id is an id from the database, meaning that the card was already from the database, and was not just created
	//TODO: if new card, get new id from database
	//TODO: test if when the cards are being created in the db and hte new ids being sent back to the javascrpit if there are any duplicate id errors.

	this.getFront = function() {
		return this.front;
	};
	this.setFront = function(newFront) {
		this.front = newFront;
		document.getElementById("cardFront"+this.number).value = this.front;
	};
	this.getBack = function() {
		return this.back;
	};
	this.setBack = function(newBack) {
		this.back = newBack;
		document.getElementById("cardBack"+this.number).value = this.back;
	};

	this.getCardId = function() {
		return this.cardId;
	};
	this.setCardId = function(aCardId) {
		this.cardId = aCardId;
		//alert("the card with number" + number + " is having its id set to " + this.cardId);
	};
	this.getHTML = function() {
		return "<li> \
					<textarea name='front+"+this.number+"' class='card' id='cardFront"+this.number+"' type='text' value='"+this.front+"' onKeyUp='updateCardFront("+this.number+", this.value)' placeholder='"+frontPlaceHolder+"'>"+this.front+"</textarea> \
					<textarea name='back"+this.number+"' class='card' id='cardBack"+this.number+"' type='text' value='"+this.back+"' onKeyUp='updateCardBack("+this.number+", this.value)' placeholder='"+backPlaceHolder+"'>"+this.back+" </textarea>\
					<button class='button' id='button"+this.number+"' onClick='deleteCells(this.value)' value='"+this.number+"'>X</button> \
					<input type='hidden' id = 'cardId"+this.number+"' name = 'cardId' value='"+this.cardId+"'> \
				</li>";
	};			//<!-- TODO: make textarea autoresize as you type -->
					//<!-- TODO: resize the height of one card if you resize the height of another
}

//Key Bindings
document.onkeydown = checkKey;

function checkKey() {
	if (window.event && window.event.keyCode == "9" && window.event.keyCode != "16") {
		var number = houseOfCards.length-1;
		if (document.getElementById("cardBack"+number.toString()) === document.activeElement){
			createNewCells();
		}
	}
}
//TODO: When someone hits tab on the last card, it shouldn't reset their focus to the deckName

//JAVASCRIPT FOR EDITDECK.PHP specifically
	var lastTimeSaved; //seconds = new Date().getTime() / 1000; //....will give you the seconds since midnight, 1 Jan 1970
	var loadedDeck = {};
			function loadDeck(callback){				
				$.ajax(
					{url:"loadDeck.php", 
			        success:function(result){
			        	loadedDeck = result;
			        	objLoadedDeck = jQuery.parseJSON(loadedDeck);
			        	numCards = Object.keys(objLoadedDeck).length;
			        	for (var i =0; i < numCards; i++){
						//	alert(i + ": " + objLoadedDeck[i].front);
			        		houseOfCards.push(new Card(objLoadedDeck[i].front, objLoadedDeck[i].back, objLoadedDeck[i].id));

			        			var html = "";
								for (j in houseOfCards) {
								html = html + houseOfCards[j].getHTML();
								}
								document.getElementById("cards").innerHTML = html;
			        	}
			        	callback();
			        }, 
			        error: function(abc) {
						 alert(abc.statusText);
			        }, 
			        data: {deckId: theDeckId}//userId is passed via Session handling
			      }
				);

			}
			function getAllCardsJSON(){
				var cards = {};
				for(var i = 0; i < houseOfCards.length; i++){
				    var number = i;
				    if(number in cards == false){
				        cards[number] = {}; // must initialize the sub-object, otherwise will get 'undefined' errors
				    }
				        cards[number]["front"] = houseOfCards[i].getFront();
				        cards[number]["back"] = houseOfCards[i].getBack();
				        cards[number]["cardId"] = houseOfCards[i].getCardId();
				}
				return JSON.stringify(cards);
			}

			function unenrollCardFromDeck(id){
				$.ajax(
					{
			        url:"unenrollCardFromDeck.php", 
			        success:function(result){
			        }, 
			        error: function(abc) {
			        	alert("something went wrong");
			        }, 
			        data: {cardId: id, deckId: theDeckId}//userId is passed via Session handling
			      }
					);
			}

			function save(){
				$("#saving").text(" . . . saving");

				$.ajax(
			      {
			        url:"saveEditDeck.php", 
			        success:function(result){
			        	alert(result);
			        	lastTimeSaved = new Date().getTime() / 1000; //....will give you the seconds since midnight, 1 Jan 1970
			            $("#saveTime").text("Deck saved 0 minutes ago");
			            $("#testingArea").append(result);
			        }, 
			        error: function(abc) {
			       	  $("#saveTime").html("Deck last saved <span id = 'lastSavedWhen'>" + lastSavedWhen() + "</span>");
			       	  $("#saveAlert").html("Something went wrong with saving. Error: \"" + abc.statusText + "\"");
			        }, 
			        data: {deck: getAllCardsJSON(), deckName: $("#deckName").text(), deckDescription: $("#deckDescription").text(), deckId: theDeckId, subjectId: subjectId }//userId is passed via Session handling
			      }
				);
				//TODO: maybe clean up some of these savetime functions and logic.
				$("#saving").text("");

			}

			function lastSavedWhen(){
				var lastSavedAgo = (new Date().getTime() / 1000) - lastTimeSaved;

				if (lastSavedAgo >= 60 * 60) //>= one hour
					return convertMinutes(lastSavedAgo);
				return lastSavedAgo * 60 + " minutes ago. ";
			}

			function convertMinutes(timeInSeconds){
				var hours = 0;
				var minutes = timeInSeconds * 60;

				while(minutes >= 60){
					hours+=1;
					minutes-=60;
				}

				return hours + " hours " + minutes + " minutes ago. ";
			}

			function updateLastSaveTime(){
				$("$lastSavedWhen").text(lastSavedWhen());
			}
			//$(document).ready(function(){
			// 	$("#save").click(

				var idOfCard;
			function setIdOfCard(value){
				idOfCard = value;
				//alert("this is setting idOfcard to: " + value);
			}
			function getIdOfCard(){
				return idOfCard;
			}
			function createCardInDb(front, back, cardNumber){
				$.ajax(
				{url:"createCards.php", 
		        success:function(result){//result is the id of the card just created, since for numberOfCards in data we put "1"
		        	//return the id of the card created
		        	$('#tempCardId').val(result);
		        	return result;
		        }, 
		        error: function(abc) {
					 alert(abc.statusText);
		        }, 
		        data: {deckId: theDeckId, numberOfCards:1, front: front, back: back}//userId is passed via Session handling
		      }
			);
			}

			$(document).ready(function(){

				loadDeck(checkNoCards); //checkNoCa
				//checkNoCards();
				$("#save").click(function(){
					save(); 

					}
					);
				}
			);