<?php
//getDecks.php
require_once '../../includes/functions.php';
session_start();

$bottomLimit = intval($_GET['bottomLimit']);//make sure it is an int. May want to use POST to be more secure to prevent sql injection from a script, if that is possible, etc. . .
$maxNumberOfDecksToReturn = intval($_GET['maxNumberOfDecksToReturn']);//make sure it is an int. May want to use POST to be more secure to prevent sql injection from a script, if that is possible, etc. . .
$subjectId =intval($_GET['subjectId']);
$userId = $_SESSION['user_id'];

		//why removing DISTINCT on this query multiplies the results of the query, i have no idea. .  .
		$sql = "SELECT DISTINCT d.id d_id, d.name d_name, d.description d_description, deu.progress deu_progress
				FROM decks d INNER JOIN deck_enrollments_users deu ON d.id = deu.id INNER JOIN decks_in_subjects dis ON d.id = dis.subject_id
				WHERE deu.user_id = :userId AND dis.subject_id = :subjectId AND dis.deck_id = d.id
				LIMIT $maxNumberOfDecksToReturn OFFSET $bottomLimit"; // this is a poorly functioning query. run it in phpmyadmin with out the DISTINCT at the beginning and you can see why. someone please fix it.
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':userId' => $userId, ':subjectId' => $subjectId/*, ':bottomLimit' => $bottomLimit*/));

		//TODO only display edit button if if the user's userId is an editors ID.oiyo
		while ($row = $stmt->fetch()){
			//TODO: make this return json, rather than preformatted html. That would be much more proper.
		echo "
					<tr class = 'deckRow'>
						<td><a href='studyDeck.php?deckId=$row[d_id]&subjectId=$subjectId&deckName=$row[d_name]'>$row[d_name]</a></td>
						<td><a href='studyDeck.php?deckId=$row[d_id]&subjectId=$subjectId&deckName=$row[d_name]'>$row[d_description]</a></td>
						<td><a href='studyDeck.php?deckId=$row[d_id]&subjectId=$subjectId&deckName=$row[d_name]'>$row[deu_progress]</a></td>
						<td><a  href='editDeck.php?deckId=$row[d_id]&subjectId=$subjectId&deckName=$row[d_name]'><img src=".  $URL_ROOT ."/img/edit.jpg title='Edit your subject'></a></td>
					</tr>";
		} 
		//DO pass the subject id's over inside the get request. will be utilized later




		//TODO: maybe pass all things things over POST, instead of GET, or find some other more user friendly way to do it, because we don't want the entire URL to be messy for users.


		$dbh = null;

