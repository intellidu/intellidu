<?php
require_once '../../includes/functions.php';
session_start();

$startingSubjectNumber = intval($_GET['startingSubjectNumber']);//make sure it is an int. May want to use POST to be more secure to prevent sql injection from a script, if that is possible, etc. . .
$maxNumberOfSubjectsToReturn = intval($_GET['maxNumberOfSubjectsToReturn']);//make sure it is an int. 
$subjectEnrollmentType = $_GET['subjectEnrollmentType'];
$returnHTML = "";


//For use in two queries.
$falseSql = "SELECT s.name s_name, s.description s_description, s.id s_id,
		(SELECT count(e.id) from subject_enrollments_users e where e.subject_id = s.id) s_numberEnrolled 
		FROM subjects s INNER JOIN subject_enrollments_users se ON s.id = se.subject_id 
		WHERE se.user_id != :userId ORDER BY progress, name, description
					LIMIT $maxNumberOfSubjectsToReturn OFFSET $startingSubjectNumber";



/*May want to use POST to be more secure to prevent sql injection from a script, if that is possible, etc. . .*/
if(isset($_SESSION['user_id'])){//only get subjects if user is logged in
	$userId = $_SESSION['user_id'];

	if($subjectEnrollmentType == 'enrolled'){
		//A. Return subjects that the user is enrolled in
		$sql = "SELECT s.name s_name, s.description s_description, s.id s_id, se.progress se_progress,
		(SELECT count(e.id) from subject_enrollments_users e where e.subject_id = s.id) s_numberEnrolled 
		FROM subjects s INNER JOIN subject_enrollments_users se ON s.id = se.subject_id 
		WHERE se.user_id = :userId ORDER BY progress, name, description
					LIMIT $maxNumberOfSubjectsToReturn OFFSET $startingSubjectNumber";
		$stmt = $dbh->prepare($sql);
		$stmt->execute(array(':userId' => $userId/*, ':startingSubjectNumber' => $startingSubjectNumber*/));
	}

	else if ($subjectEnrollmentType == 'not_enrolled'){
		//B. Return the subjects that the user is not enrolled in.
		$stmt = $dbh->prepare($falseSql);
		$stmt->execute(array(':userId' => $userId/*, ':startingSubjectNumber' => $startingSubjectNumber*/));

	}
		//echo $getAllSubjects . " is the value of get all subjects <br> ";
		//TODO: fix shakiness when mousing over a border. especially when nearby it changes the number of rows of text within a table row
		//echo "<div id='tableData'>";
	while ($row = $stmt->fetch()){

		if($subjectEnrollmentType == 'enrolled'){
			$progressColumn = "<td><a class='noFormatting' href='viewSubject.php?subjectId=$row[s_id]'>$row[se_progress]</a></td>";
			$enrollmentChange = "<td id='unenrollFromSubject'><a href='changeEnrollment.php?subjectId=$row[s_id]&action=unenroll&type=user'><!--TODO: don't let users enroll as users and editors via GET request. Change to POST somehow --><img src=$URL_ROOT/img/remove.jpg title='Unenroll'></a></td>";
		}

		elseif($subjectEnrollmentType == 'not_enrolled'){
			$progressColumn = "<td><a class='noFormatting' href='viewSubject.php?subjectId=$row[s_id]'>No Progress yet</a></td>";
			$enrollmentChange = "<td id ='enrollbutton$row[s_id]' class ='noFormatting enroll'><a href='changeEnrollment.php?subjectId=$row[s_id]&action=enroll&type=user'><img src=$URL_ROOT/img/add.png title='Enroll'></a></td>";
		}
		
		$returnHTML.= "
					<tr class='subjectRow' id ='$row[s_id]'>
						<td><a class='noFormatting' href='viewSubject.php?subjectId=$row[s_id]'>$row[s_name]</a></td>
						<td><a class='noFormatting' href='viewSubject.php?subjectId=$row[s_id]'>$row[s_description]</a></td>"
						. $progressColumn . 
						"<td><a class='noFormatting' href='viewSubject.php?subjectId=$row[s_id]'>$row[s_numberEnrolled]</a></td>"
						. $enrollmentChange .
					"</tr>";
	//echo "</div>";//^^TODO: there should be a way to just have one link up there and make the clicking work, but somehow I can't figure out how to do that, so I have to have 4 a tags.
					//TODO: Make it so that you can click anywhere in the row datas, and not just the text.
						//TODO: Mobile ^^ Todo that user would swipe left to unenroll
					//TODO: Make it so that mousing over the unenroll button doesn't highlight the entire row.
	}
} 
else /*if the user is not logged in, then return the subjects they are not enrolled in, if that is what the command is. This is just code copied and pasted from above.*/
{
	if ($subjectEnrollmentType == 'not_enrolled'){
		//B. Return the subjects that the user is not enrolled in.
		$stmt = $dbh->prepare($falseSql);
		$stmt->execute(array(':userId' => $userId/*, ':startingSubjectNumber' => $startingSubjectNumber*/));

		while ($row = $stmt->fetch()){

			$progressColumn = "0";
			$enrollmentChange = "";// you must be logged in to enroll

			$returnHTML.= "
					<tr class='subjectRow' id ='$row[s_id]''>
						<td><a class='noFormatting' href='viewSubject.php?subjectId=$row[s_id]'>$row[s_name]</a></td>
						<td><a class='noFormatting' href='viewSubject.php?subjectId=$row[s_id]'>$row[s_description]</a></td>"
						. $progressColumn . 
						"<td><a class='noFormatting' href='viewSubject.php?subjectId=$row[s_id]'>$row[s_numberEnrolled]</a></td>"
						. $enrollmentChange .
					"</tr>";
		}
	}		
}

	echo $returnHTML . $subjectEnrollmentType;

    $dbh = null;